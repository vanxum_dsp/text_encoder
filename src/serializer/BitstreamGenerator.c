/**
 * Copyright 2015 Xi'an Vanxum Electronics Technology Co.,Ltd. All Rights Reserved.
 *
 * Created by Derui on 2015/10/28.
 *
 * 定义码流输入相关函数
 */

#include "vgtp/serializer/BitstreamGenerator.h"
#include "drive/mem_drive.h"
#include "var/var_text_encoder.h"
#include <stdlib.h>
#include <string.h>

extern unsigned char aBitstream_data[8*1024*1024];
extern unsigned char extendBitstream_data[FRAME_WIDTH*FRAME_HEIGHT*3*2];
int bitstreamInitialize(InputBitstream *bitstream, const uint32_t initial_capacity) {
    //bitstream->data = (unsigned char *) Drv_malloc(initial_capacity * sizeof(unsigned char));//initial_capacity = FRAME_WIDTH*FRAME_HEIGHT*3
	bitstream->data = aBitstream_data;
    bitstream->capacity = initial_capacity;
    bitstream->length = 0;
    bitstream->buffer = 0;
    bitstream->buffer_size = 0;
    return 0;
}

int bitstreamFinalize(InputBitstream *bitstream) {
    if (bitstream->buffer_size > 0) {
        return flushBuffer(bitstream);
    } else {
        return 0;
    };
}

int bitstreamPutBit(InputBitstream *bitstream, const int bit_value) {
    bitstream->buffer <<= 1;
    bitstream->buffer |= (bit_value & 1);
    bitstream->buffer_size++;
    if (bitstream->buffer_size == kCharLength) {
        return flushBuffer(bitstream);
    }
    return 0;
}

int bitstreamPutInt(InputBitstream *bitstream, const unsigned int bit_length, const unsigned int val)
{
    const int mask = 0x1;
    int bit_value;
    int i;
    for (i = bit_length - 1; i >= 0; i--) {
        bit_value = ((val >> i) & mask) != 0;
        bitstreamPutBit(bitstream, bit_value);
    }
    return 0;
}

int bitstreamPutSignedInt(InputBitstream *bitstream, const unsigned int bit_length, const int val) {
    if (val >= 0) {
        return bitstreamPutInt(bitstream, bit_length, (const unsigned int) val);
    } else {
        if (bit_length < 32) {
            const unsigned int converted_value = (unsigned int) val + (1 << bit_length);
            return bitstreamPutInt(bitstream, bit_length, converted_value);
        } else if (bit_length == 32) {
            return bitstreamPutInt(bitstream, bit_length, (const unsigned int) val);
        }
    }
    return 0;
}

int bitstreamDestroy(InputBitstream *bitstream) {
    Drv_free(bitstream->data);
    return 0;
}

int bitstreamPutCharArray(InputBitstream *bitstream, const uint32_t data_length_in_byte, const unsigned char *data) {
    const uint32_t required_capacity = bitstream->length + data_length_in_byte;
    while (required_capacity > bitstream->capacity) {
        extendBitstream(bitstream);
    }
    memcpy(bitstream->data + bitstream->length, data, data_length_in_byte);
    bitstream->length += data_length_in_byte;
    return 0;
}

static int flushBuffer(InputBitstream *bitstream) {//�ռ���һ���ֽ���д������
    const int remain_bit = kCharLength - bitstream->buffer_size;
    if (remain_bit > 0) {
        bitstream->buffer <<= remain_bit;
    }
    if (bitstream->length == bitstream->capacity) {
        //extendBitstream(bitstream);//������
    }
    (bitstream->data)[bitstream->length++] = bitstream->buffer;
    bitstream->buffer = 0;
    bitstream->buffer_size = 0;
    return 0;
}

static int extendBitstream(InputBitstream *bitstream) {
    bitstream->data = Drv_realloc(bitstream->data, sizeof(unsigned char) * bitstream->capacity * 2);//FRAME_WIDTH*FRAME_HEIGHT*3*2
	//bitstream->data = extendBitstream_data;
	bitstream->capacity *= 2;
    return 0;
}
