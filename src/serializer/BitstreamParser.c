/**
 * Copyright 2015 Xi'an Vanxum Electronics Technology Co.,Ltd. All Rights Reserved.
 *
 * Created by Derui on 2015/10/28.
 *
 * 定义码流读取相关函数
 */

#include <vgtp/serializer/BitstreamParser.h>

void outputBitstreamInitialize(unsigned char *data, uint32_t length, OutputBitstream *output_bitstream) {
    output_bitstream->buffer = 0;
    output_bitstream->buffer_size = 0;
    output_bitstream->data = data;
    output_bitstream->length = length;
    output_bitstream->position = 0;
}

bool outputBitstreamGetBit(OutputBitstream *bitstream, int *result) {
    if (bitstream->buffer_size == 0) {
        bool success = outputBitstreamLoadBuffer(bitstream);
        if (!success) {
            return false;
        }
    }
    const int mask = 0x1;
    *result = ((bitstream->buffer) >> (bitstream->buffer_size-- - 1)) & mask;
    return true;
}

bool outputBitstreamGetIntUnsigned(OutputBitstream *bitstream, unsigned int bit_length, uint32_t *result) {
    uint32_t value = 0;
    int temp;
    unsigned int i;
    for (i = 0; i < bit_length; i++) {
        if (outputBitstreamGetBit(bitstream, &temp)) {
            value <<= 1;
            value |= temp;
        } else {
            return false;
        }
    }
    *result = value;
    return true;
}

bool outputBitstreamGetIntSigned(OutputBitstream *bitstream, unsigned int bit_length, int32_t *result) {
    uint32_t value = 0;
    int temp;
    unsigned int i;
    for (i = 0; i < bit_length; i++) {
        if (outputBitstreamGetBit(bitstream, &temp)) {
            value <<= 1;
            value |= temp;
        } else {
            return false;
        }
    }
    uint32_t mask = 1u << (bit_length - 1);
    if (value & mask) {
        if (bit_length == 32) {
            *result = (int32_t) value; //这是为了避免overflow出现
        } else {
            *result = (int32_t) value - (mask << 1);
        }
    } else {
        *result = (int32_t) value;
    }
    return true;
}
static bool outputBitstreamLoadBuffer(OutputBitstream *bitstream) {
    if (bitstream->position != bitstream->length) {
        bitstream->buffer = (bitstream->data)[(bitstream->position)++];
        bitstream->buffer_size = 8;
        return true;
    } else {
        return false;
    }
}
