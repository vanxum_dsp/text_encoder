//#include "types.h"
#include "stddef.h"  
#include "string.h"  
#include <stdio.h>
/* Imported functions */  
//extern void prom_printf (char *fmt, ...);  

static char *malloc_ptr = 0;  
static char *malloc_top = 0;  
static char *last_alloc = 0;  
  
void Drv_malloc_init(char *bottom, unsigned long size)
{  
        malloc_ptr = bottom;  
        malloc_top = bottom + size;  
}  
  
void Drv_malloc_dispose(void)
{  
        malloc_ptr = 0;  
        last_alloc = 0;  
}  
  
void *Drv_malloc (unsigned int size)
{  
    char *caddr;  
  
    if (!malloc_ptr)  
        return NULL;  
    if ((malloc_ptr + size + sizeof(int)) > malloc_top) {  
        printf("malloc failed\n");  
        return NULL;  
    }  
    *(int *)malloc_ptr = size;  
    caddr = malloc_ptr + sizeof(int);  
    malloc_ptr += size + sizeof(int);  
    last_alloc = caddr;  
    malloc_ptr = (char *) ((((unsigned int) malloc_ptr) + 3) & (~3));  
    return caddr;  
}  

void *Drv_calloc (unsigned int n,unsigned int size)
{
	char *caddr; 
	 if (!malloc_ptr)  
     return NULL; 
	 if ((malloc_ptr + n*size + sizeof(int)) > malloc_top) {  
        printf("Drv_malloc failed\n");  
        return NULL;  
    }  
	*(int *)malloc_ptr = n*size;  
    caddr = malloc_ptr + sizeof(int); 
	memset(caddr,0,n*size);
    malloc_ptr += n*size + sizeof(int);  
    last_alloc = caddr;  
    malloc_ptr = (char *) ((((unsigned int) malloc_ptr) + 3) & (~3));  
    return caddr;
}
  
void *Drv_realloc(void *ptr, unsigned int size)
{  
    char *caddr, *oaddr = ptr;  
  
    if (!malloc_ptr)  
        return NULL;  
    if (oaddr == last_alloc) {  
        if (oaddr + size > malloc_top) {  
                printf("Drv_realloc failed\n");  
                return NULL;  
        }  
        *(int *)(oaddr - sizeof(int)) = size;  
        malloc_ptr = oaddr + size;  
        return oaddr;  
    }  
    caddr = Drv_malloc(size);
    if (caddr != 0 && oaddr != 0)  
        memcpy(caddr, oaddr, *(int *)(oaddr - sizeof(int)));  
    return caddr;  
}  
  
void Drv_free (void *m)
{  
    if (!malloc_ptr)  
        return;  
    if (m == last_alloc)  
        malloc_ptr = (char *) last_alloc - sizeof(int);  
}  
  
void Drv_mark (void **ptr)
{  
    if (!malloc_ptr)  
        return;  
    *ptr = (void *) malloc_ptr;  
}  
  
void Drv_release (void *ptr)
{  
    if (!malloc_ptr)  
        return;  
    malloc_ptr = (char *) ptr;  
}  
  
char *Drv_strdup(char const *str)
{  
    char *p = Drv_malloc(strlen(str) + 1);
    if (p)  
         strcpy(p, str);  
    return p;  
}  
