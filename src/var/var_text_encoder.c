//全局变量定义
#include "vgtp/recognize/RecognizeResult.h"
#include "var/var_text_encoder.h"
#include "core/image/Pixel.h"
#include "vgtp/codec/text_codec/encoder/PixelGrid.h"
#include "vgtp/codec/text_codec/encoder/BaseColor.h"
#include "vgtp/codec/text_codec/TextCodecConfig.h"

#pragma DATA_SECTION(Mem_malloc,  ".MemAlloc");
#pragma DATA_ALIGN(Mem_malloc, 4);
char Mem_malloc[0x40000000];

#pragma DATA_SECTION(mb_recognize_info,  ".TextEncoder_var");
#pragma DATA_ALIGN(mb_recognize_info, 4);
enum MBEncodeType mb_recognize_info[8160]; //宏块为16*16, 1080p图像共8160个宏块

#pragma DATA_SECTION(frame_data1,  ".TextEncoder_var");
#pragma DATA_ALIGN(frame_data1, 4);
unsigned char frame_data1[FRAME_WIDTH*FRAME_HEIGHT];

#pragma DATA_SECTION(frame_data2,  ".TextEncoder_var");
#pragma DATA_ALIGN(frame_data2, 4);
unsigned char frame_data2[FRAME_WIDTH*FRAME_HEIGHT];

#pragma DATA_SECTION(frame_data3,  ".TextEncoder_var");
#pragma DATA_ALIGN(frame_data3, 4);
unsigned char frame_data3[FRAME_WIDTH*FRAME_HEIGHT];

#pragma DATA_SECTION(block_data_component1,  ".TextEncoder_var");
#pragma DATA_ALIGN(block_data_component1, 4);
int block_data_component1[FRAME_WIDTH*FRAME_HEIGHT];

#pragma DATA_SECTION(block_data_component2,  ".TextEncoder_var");
#pragma DATA_ALIGN(block_data_component2, 4);
int block_data_component2[FRAME_WIDTH*FRAME_HEIGHT];

#pragma DATA_SECTION(block_data_component3,  ".TextEncoder_var");
#pragma DATA_ALIGN(block_data_component3, 4);
int block_data_component3[FRAME_WIDTH*FRAME_HEIGHT];

#pragma DATA_SECTION(aQuantized_image,  ".TextEncoder_var");
#pragma DATA_ALIGN(aQuantized_image, 4);
Pixel aQuantized_image[FRAME_WIDTH*FRAME_HEIGHT];

#pragma DATA_SECTION(aQuantized_image_index,  ".TextEncoder_var");
#pragma DATA_ALIGN(aQuantized_image_index, 4);
QuantizeIndex aQuantized_image_index[FRAME_WIDTH*FRAME_HEIGHT];

#pragma DATA_SECTION(aBase_color_map,  ".TextEncoder_var");
#pragma DATA_ALIGN(aBase_color_map, 4);
BaseColor aBase_color_map[8160];

#pragma DATA_SECTION(aEstimate_symbols,  ".TextEncoder_var");
#pragma DATA_ALIGN(aEstimate_symbols, 4);
EstimateSymbol aEstimate_symbols[FRAME_WIDTH*FRAME_HEIGHT];

#pragma DATA_SECTION(aCoding_result,  ".TextEncoder_var");
#pragma DATA_ALIGN(aCoding_result, 4);
int aCoding_result[FRAME_WIDTH/PIXEL_GROUP_COUNT*FRAME_HEIGHT];

#pragma DATA_SECTION(aPixel_group_level_coding_result,  ".TextEncoder_var");///BUYAO
#pragma DATA_ALIGN(aPixel_group_level_coding_result, 4);
int aPixel_group_level_coding_result[FRAME_WIDTH*FRAME_HEIGHT];

#pragma DATA_SECTION(aBitstream_data,  ".TextEncoder_var");
#pragma DATA_ALIGN(aBitstream_data, 4);
unsigned char aBitstream_data[8*1024*1024];//�Ȱ�8M����FRAME_WIDTH*FRAME_HEIGHT*3

//#pragma DATA_SECTION(extendBitstream_data,  ".TextEncoder_var");
//#pragma DATA_ALIGN(extendBitstream_data, 4);
//unsigned char extendBitstream_data[FRAME_WIDTH*FRAME_HEIGHT*3*2];

#pragma DATA_SECTION(aEstimate_map_data,  ".TextEncoder_var");
#pragma DATA_ALIGN(aEstimate_map_data, 4);
unsigned char aEstimate_map_data[1024];//1021

#pragma DATA_SECTION(aBase_color_bitstream_data,  ".TextEncoder_var");
#pragma DATA_ALIGN(aBase_color_bitstream_data, 4);
unsigned char aBase_color_bitstream_data[24480];

#pragma DATA_SECTION(aHuff_dict_stream_data,  ".TextEncoder_var");
#pragma DATA_ALIGN(aHuff_dict_stream_data, 4);
unsigned char aHuff_dict_stream_data[1024];

#pragma DATA_SECTION(aEscape_colors,  ".TextEncoder_var");
#pragma DATA_ALIGN(aEscape_colors, 4);
int aEscape_colors[FRAME_WIDTH*FRAME_HEIGHT*3];
