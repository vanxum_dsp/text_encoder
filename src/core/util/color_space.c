/**
 * Copyright 2015 Xi'an Vanxum Electronics Technology Co.,Ltd. All Rights Reserved.
 *
 * Created by ChrisChung on 2015/11/2.
 *
 * 文件描述 
 */

#include "core/util/color_space.h"


/***
 * 将一个int 数字限定在0-255的范围
 */
int clip_sh_0_255(int in){
    int out = MAX(in,0);
    out = MIN(255,out);
    return out;
}

/***
 *
 * B = 1.164(Y - 16)+ 2.018(U - 128)
 * G = 1.164(Y - 16) - 0.813(V - 128) - 0.391(U - 128)
 * R = 1.164(Y - 16) + 1.596(V - 128)
 */
void YUV444_TO_RGB(const YUVPixel *yuv,RGBPixel *rgb){
    rgb->R = (int)(1.164f*(yuv->Y - 16) + 1.596f * (yuv->V-128));
    rgb->R = clip_sh_0_255(rgb->R);
    rgb->G = (int)(1.164f*(yuv->Y - 16) - 0.813f*(yuv->V-128) - 0.391f * (yuv->U-128));
    rgb->G = clip_sh_0_255(rgb->G);
    rgb->B = (int)(1.164f*(yuv->Y - 16) + 2.018f * (yuv->U-128));
    rgb->B = clip_sh_0_255(rgb->B);
}
