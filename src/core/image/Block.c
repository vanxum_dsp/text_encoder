/**
 * Copyright 2015 Xi'an Vanxum Electronics Technology Co.,Ltd. All Rights Reserved.
 *
 * Created by ChrisChung on 2015/11/2.
 *
 * Block结构体相关函数实现
 */

#include "core/image/Block.h"
#include "drive/mem_drive.h"
#include <stdlib.h>
#include <stdio.h>

void initializeBlock(const enum ColorSpaceFormat format, const unsigned int width, const unsigned int height,
                     struct Block *block) {
    if (format == kYUV444) {
        block->color_space_format = kYUV444;
        block->width = width;
        block->height = height;
        block->data_component1 = (unsigned char *) Drv_calloc(width * height, sizeof(unsigned char));
        block->data_component2 = (unsigned char *) Drv_calloc(width * height, sizeof(unsigned char));
        block->data_component3 = (unsigned char *) Drv_calloc(width * height, sizeof(unsigned char));
        block->stride = width;
    } else {
        //当前不支持其它格式图像
    }

}
bool getPixelFromBlock(const struct Block *block, const int coordinate_x, const int coordinate_y,
                       struct Pixel *output) {
    if (coordinate_x >= block->width || coordinate_y >= block->height) {
        return false;
    }
    switch(block->color_space_format){
        case kYUV444 :{
            int pixel_ptr_offset = coordinate_y * block->stride + coordinate_x;
            output->component1 = (int) block->data_component1[pixel_ptr_offset];//Y
            output->component2 = (int) block->data_component2[pixel_ptr_offset];//U
            output->component3= (int) block->data_component3[pixel_ptr_offset];//V
            break;
        }
        default: {
            fprintf(stderr, "Failed to get pixel from block. Only YUV444 format is currently supported.\n");
        }
        //Todo:other color space format
    }
    return true;
}

void freeBlockMemory(struct Block *block) {
    if (block) {
        if (block->data_component1) {
            Drv_free(block->data_component1);
            block->data_component1 = NULL;
        }
        if (block->data_component2) {
            Drv_free(block->data_component2);
            block->data_component2 = NULL;
        }
        if (block->data_component3) {
            Drv_free(block->data_component3);
            block->data_component3 = NULL;
        }
    }
}

bool getMBInfo(const unsigned int total_width, const unsigned int total_height, const unsigned int block_size,
               const unsigned int block_coord_x,
               const unsigned int block_coord_y, struct MBInfo *mb_info) {
    const unsigned int mb_start_pixel_coord_x = block_coord_x * block_size;
    const unsigned int mb_start_pixel_coord_y = block_coord_y * block_size;

    //如果指定的宏块不存在，返回失败
    if (mb_start_pixel_coord_x >= total_width || mb_start_pixel_coord_y >= total_height) {
        return false;
    }

    mb_info->first_element_offset = mb_start_pixel_coord_y * total_width + mb_start_pixel_coord_x;
    mb_info->width = (total_width - mb_start_pixel_coord_x < block_size) ? total_width - mb_start_pixel_coord_x
                                                                         : block_size;
    mb_info->height = (total_height - mb_start_pixel_coord_y < block_size) ? total_height - mb_start_pixel_coord_y
                                                                           : block_size;
    return true;
}
