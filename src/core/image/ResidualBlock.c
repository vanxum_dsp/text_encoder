/**
 * Copyright 2015 Xi'an Vanxum Electronics Technology Co.,Ltd. All Rights Reserved.
 *
 * Created by Derui on 2015/11/27.
 *
 * 定义残差图像相关函数
 */

#include "core/image/ResidualBlock.h"
#include "drive/mem_drive.h"
#include "var/var_text_encoder.h"
#include <stdlib.h>

extern int block_data_component1[FRAME_WIDTH*FRAME_HEIGHT];
extern int block_data_component2[FRAME_WIDTH*FRAME_HEIGHT];
extern int block_data_component3[FRAME_WIDTH*FRAME_HEIGHT];

void initializeResidualBlock(const enum ColorSpaceFormat format, const unsigned int width, const unsigned int height,
                             struct ResidualBlock *block) {
    if (format == kYUV444) {
        block->color_space_format = kYUV444;
        block->width = width;
        block->height = height;
        //block->data_component1 = (int *) Drv_calloc(width * height, sizeof(int));
        //block->data_component2 = (int *) Drv_calloc(width * height, sizeof(int));
        //block->data_component3 = (int *) Drv_calloc(width * height, sizeof(int));
		block->data_component1 = (int *)block_data_component1;
        block->data_component2 = (int *)block_data_component2;
        block->data_component3 = (int *)block_data_component3;
        block->stride = width;
    } else {
        //当前不支持其它格式图像
    }

}

bool getPixelFromResidualBlock(const struct ResidualBlock *block, const int coordinate_x, const int coordinate_y,
                               struct Pixel *output) {
    if (coordinate_x >= block->width || coordinate_y >= block->height) {
        return false;
    }
    switch (block->color_space_format) {
        case kYUV444 : {
            int pixel_ptr_offset = coordinate_y * block->stride + coordinate_x;
            output->component1 = block->data_component1[pixel_ptr_offset];//Y
            output->component2 = block->data_component2[pixel_ptr_offset];//U
            output->component3 = block->data_component3[pixel_ptr_offset];//V
            break;
        }
            //Todo:other color space format
    }
    return true;
}

void freeResidualBlockMemory(struct ResidualBlock *block) {
    if (block) {
        if (block->data_component1) {
            Drv_free(block->data_component1);
            block->data_component1 = NULL;
        }
        if (block->data_component2) {
            Drv_free(block->data_component2);
            block->data_component2 = NULL;
        }
        if (block->data_component3) {
            Drv_free(block->data_component3);
            block->data_component3 = NULL;
        }
    }
}


