//
// Created by Derui on 2015/6/29.
//

#include "vgtp/codec/text_codec/encoder/BitStreamGenerator.h"
#include "vgtp/codec/text_codec/TextCodecConfig.h"
#include "var/var_text_encoder.h"
#include <math.h>
#include <vgtp/serializer/BitstreamGenerator.h>
//#include <core/log/log.h>

#define ByteLength 8
extern unsigned char aEstimate_map_data[8160];
extern unsigned char aBase_color_bitstream_data[24480];
extern unsigned char aHuff_dict_stream_data[512*1024];
extern int aEscape_colors[FRAME_WIDTH*FRAME_HEIGHT*3];
void generateStream(const int num_pixels, const unsigned int num_text_mb, const struct BaseColor *base_color_map,
                    const struct BaseColorDimension *base_color_dimension, const struct Pixel *quantized_image,
                    const enum EstimateSymbol *estimate_symbols, const struct QuantizeIndex *quantized_image_index,
                    const int *pixel_group_level_coding_result, const int size_pixel_group_level_coding_result,
                    struct RawBitstream *bitstream, struct StreamStats *stream_stats) {
    const int kBitstreamInitialSize = num_pixels * 3; //1920*1080*3,//7*1024*1024
    InputBitstream input_bitstream;
    bitstreamInitialize(&input_bitstream, (uint32_t) kBitstreamInitialSize);

    int base_color_num;
    //码流 宏块数量 1比特
    bitstreamPutInt(&input_bitstream, ByteLength, num_text_mb);
    if (num_text_mb >= 1) {
//        printf("%d blocks detected.\n", num_text_mb);
        base_color_num = BASE_COLOR_NUMBER;

        codeBaseColors(base_color_map, base_color_dimension, base_color_num, &input_bitstream);


        bitstreamFinalize(&input_bitstream); //End of Base color section
        const uint32_t size_after_base_color = input_bitstream.length;

        /* Pixel Group Level Data */
        codePixelGroupLevelData(pixel_group_level_coding_result, size_pixel_group_level_coding_result,
                                &input_bitstream);

        const uint32_t size_after_pixel_group_huff_code = input_bitstream.length;

        /* O Symbol Map Data */
        //3*3 bits for each O in estimate symbol map
        int escape_index = base_color_num;
        codeOtherInEstimateSymbols(estimate_symbols, quantized_image_index, num_pixels, escape_index, &input_bitstream);
        const uint32_t size_after_o_symbol_code = input_bitstream.length;


        //Escape color(index=EscapeColorIndex), 1 Byte for each Y/U/V component in pixel
        codeEscapePixel(quantized_image, quantized_image_index, num_pixels, escape_index, &input_bitstream);
        const int size_after_escape_pixel = input_bitstream.length;

        printf("Size of bit stream: %d Byte.\n", input_bitstream.length);
		stream_stats->size_base_color = size_after_base_color;
		stream_stats->size_pixel_group_huffcode = size_after_pixel_group_huff_code - size_after_base_color;
		stream_stats->size_o_symbol_code = size_after_o_symbol_code - size_after_pixel_group_huff_code;
		stream_stats->size_escape_color = size_after_escape_pixel - size_after_o_symbol_code;
		/*
        *stream_stats = (StreamStats) {.size_base_color = size_after_base_color, 
			.size_pixel_group_huffcode=size_after_pixel_group_huff_code - size_after_base_color,
                .size_o_symbol_code=size_after_o_symbol_code - size_after_pixel_group_huff_code,
                .size_escape_color=size_after_escape_pixel - size_after_o_symbol_code};*/


    } else {
        fprintf(stdout, "No text block detected. The program will not preceeding generating bitstream.");
        printf("Size of bit stream: %d Byte.\n", input_bitstream.length);
    }

    //input_bitstream的码流数据传入bitstream，无需释放
    bitstream->length = input_bitstream.length;
    bitstream->data = input_bitstream.data;


}
#define FREQUENCY_RANGE 256 //Escape colors take value from 0 to 255.
static void codeEscapePixel(const struct Pixel *quantized_image,
                            const struct QuantizeIndex *quantized_image_index, const int num_pixels,
                            const int escape_index, InputBitstream *bit_stream) {
    const int kBitstreamInitialSize = 6220800;
    InputBitstream escape_pixel_stream;
    bitstreamInitialize(&escape_pixel_stream, kBitstreamInitialSize);

    //获取逃逸色信息，生成逃逸色频率直方图
    //int *escape_colors = (int *) Drv_malloc(num_pixels * sizeof(int));
	int *escape_colors =aEscape_colors;
    int num_escape_colors = 0;
    int frequencies[FREQUENCY_RANGE];
    int i;
    for (i = 0; i < FREQUENCY_RANGE; i++) {
        frequencies[i] = 0;
    }

    int escape_color;
    for (i = 0; i < num_pixels; i++) {
        //Y frame
        if (quantized_image_index[i].component1 == escape_index) {
            escape_color = quantized_image[i].component1;
            escape_colors[num_escape_colors++] = escape_color;
            frequencies[escape_color]++;
        }
        //U frame
        if (quantized_image_index[i].component2 == escape_index) {
            escape_color = quantized_image[i].component2;
            escape_colors[num_escape_colors++] = escape_color;
            frequencies[escape_color]++;
        }
        //V frame
        if (quantized_image_index[i].component3 == escape_index) {
            escape_color = quantized_image[i].component3;
            escape_colors[num_escape_colors++] = escape_color;
            frequencies[escape_color]++;
        }
    }

    //使用逃逸色频率直方图生成huffman码表
    HuffCode huff_code_dict[FREQUENCY_RANGE];
    for (i = 0; i < FREQUENCY_RANGE; i++) {
        huff_code_dict[i] = NULL;
    }
    struct INode *huffman_tree_root = getHuffCodeMap(huff_code_dict, frequencies, FREQUENCY_RANGE);

    //将huffman码表写入码流
    codeHuffDictionary(huffman_tree_root, &escape_pixel_stream);

    freeHuffmanTree(huffman_tree_root);

    //使用huffman码表对逃逸色进行编码
    printf("Escape color count: %d\n", num_escape_colors);
    codeHuffBitStream(escape_colors, num_escape_colors, huff_code_dict, &escape_pixel_stream);

    //写入逃逸色码流长度  3 Byte
    bitstreamPutInt(bit_stream, ByteLength * 3, escape_pixel_stream.length);

    //写入逃逸色真实码流
    bitstreamPutCharArray(bit_stream, escape_pixel_stream.length, escape_pixel_stream.data);

    bitstreamDestroy(&escape_pixel_stream);
    Drv_free(escape_colors);
    for (i = 0; i < FREQUENCY_RANGE; i++) {
        if (huff_code_dict[i]) {
            cvector_destroy(huff_code_dict[i]);
        }
    }
}

static void codeOtherInEstimateSymbols(const enum EstimateSymbol *estimate_symbols,
                                       const struct QuantizeIndex *quantized_image_index, const int num_pixels,
                                       const int escape_index, InputBitstream *bit_stream)
{
	int i;
    const unsigned int kSymbolsCodeLength = (unsigned int) (ceil(log(escape_index + 1) / log(2)));
    const int kBitstreamInitialSize = 6220800;
    InputBitstream o_symbol_bit_stream;
    bitstreamInitialize(&o_symbol_bit_stream, kBitstreamInitialSize);

    for (i = 0; i < num_pixels; i++) {
        if (estimate_symbols[i] == Other) {
            bitstreamPutInt(&o_symbol_bit_stream, kSymbolsCodeLength,
                            (unsigned int) quantized_image_index[i].component1);
            bitstreamPutInt(&o_symbol_bit_stream, kSymbolsCodeLength,
                            (unsigned int) quantized_image_index[i].component2);
            bitstreamPutInt(&o_symbol_bit_stream, kSymbolsCodeLength,
                            (unsigned int) quantized_image_index[i].component3);
        }
    }
    bitstreamFinalize(&o_symbol_bit_stream);

    //写入O-Symbol码流长度  3 Byte
    bitstreamPutInt(bit_stream, ByteLength * 3, o_symbol_bit_stream.length);

    //写入O-Symbol真实码流
    bitstreamPutCharArray(bit_stream, o_symbol_bit_stream.length, o_symbol_bit_stream.data);

    bitstreamDestroy(&o_symbol_bit_stream);
    bitstreamFinalize(bit_stream);
}
#define KDATARANGE 84//3^4+3=84
static void codePixelGroupLevelData(const int *pixel_group_level_coding_result,
                                    const int size_pixel_group_level_coding_result, InputBitstream *out_stream)
{
	int i;
    int frequencies[KDATARANGE];
    for (i = 0; i < KDATARANGE; i++) {
        frequencies[i] = 0;
    }

    for (i = 0; i < size_pixel_group_level_coding_result; i++) {
        int result = pixel_group_level_coding_result[i];
        if (result >= KDATARANGE) {
            fprintf(stderr, "The pixel group level data: %d goes beyond the limit: %d.\n", result, KDATARANGE);
        } else {
            frequencies[result]++;
        }
    }

    HuffCode huff_code_dict[KDATARANGE];
    for (i = 0; i < KDATARANGE; i++) {
        huff_code_dict[i] = NULL;
    }
    struct INode *huffman_tree_root = getHuffCodeMap(huff_code_dict, frequencies, KDATARANGE);

    //Code Huffman dictionary into the out stream.
    codeHuffDictionary(huffman_tree_root, out_stream);

    freeHuffmanTree(huffman_tree_root);

    //Code Huffman coded bit stream
    codeHuffBitStream(pixel_group_level_coding_result, size_pixel_group_level_coding_result, huff_code_dict,
                      out_stream);

    //释放huff_code_dict的内存
    for (i = 0; i < KDATARANGE; i++) {
        if (huff_code_dict[i]) {
            cvector_destroy(huff_code_dict[i]);
        }
    }
}

static void codeHuffDictionary(struct INode *huff_tree_root, InputBitstream *out_stream) {
    if (!huff_tree_root) {
        fprintf(stderr, "Invalid huff_tree_root, unable to persist huff dictionary to file.\n");
        return;
    }
    const uint32_t kHuffDictStreamSize = 1024;  //512K should be enough.
    InputBitstream huff_dict_stream;
    //bitstreamInitialize(&huff_dict_stream, kHuffDictStreamSize);
	
	huff_dict_stream.data = aHuff_dict_stream_data;
    huff_dict_stream.capacity = kHuffDictStreamSize;
    huff_dict_stream.length = 0;
    huff_dict_stream.buffer = 0;
    huff_dict_stream.buffer_size = 0;
	
    codeHuffNodeBinary(huff_tree_root, &huff_dict_stream);
    bitstreamFinalize(&huff_dict_stream);
    //编码Huffman Dict长度  2字节
    bitstreamPutInt(out_stream, ByteLength * 2, huff_dict_stream.length);

    //编码Huffman Dict
    bitstreamPutCharArray(out_stream, huff_dict_stream.length, huff_dict_stream.data);

    //释放临时变量
    bitstreamDestroy(&huff_dict_stream);
//    PersistBitStream(huff_dict_stream, huff_dict_path_);
}

static void codeHuffNodeBinary(const struct INode *node, InputBitstream *bit_stream) {
    if (node->is_leaf) {
        bitstreamPutBit(bit_stream, 1);
        bitstreamPutInt(bit_stream, ByteLength, (unsigned int) node->value);

        return;
    } else {
        bitstreamPutBit(bit_stream, 0);
        codeHuffNodeBinary(node->left, bit_stream);
        codeHuffNodeBinary(node->right, bit_stream);
    }
}

static void codeHuffBitStream(const int *pixel_group_level_coding_result,
                              const int size_pixel_group_level_coding_result, const HuffCode *huff_code_map,
                              InputBitstream *out_stream)
{
	int i;
	citerator iter;
    InputBitstream huff_bitstream;
    bitstreamInitialize(&huff_bitstream, 10000000);
    for (i = 0; i < size_pixel_group_level_coding_result; i++) {
        HuffCode huff_code = huff_code_map[pixel_group_level_coding_result[i]];
        for (iter = cvector_begin(huff_code);
             iter < cvector_end(huff_code); iter = cvector_next(huff_code, iter)) {
            unsigned int value = *((unsigned int *) iter); //0 or 1
            bitstreamPutBit(&huff_bitstream, value);
        }
    }

    bitstreamFinalize(&huff_bitstream);

    //存入码流长度字段 3 Byte
    const int kBitStreamLengthByte = 3; //码流长度字段长度
    bitstreamPutInt(out_stream, kBitStreamLengthByte * ByteLength, huff_bitstream.length);

    //存入真实码流 可变长度
    bitstreamPutCharArray(out_stream, huff_bitstream.length, huff_bitstream.data);
    bitstreamFinalize(out_stream); //END of Huffman Bitstream

    bitstreamDestroy(&huff_bitstream);
}

static void codeBaseColors(const struct BaseColor *base_color_map,
                           const struct BaseColorDimension *base_color_dimension, const int num_base_color_in_mb,
                           InputBitstream *bit_stream)
{
	int x_index;
	int y_index;
    InputBitstream estimate_map; //基本色预测编码图
    const uint32_t kEstimateMapInitialSize =
            (base_color_dimension->x_num_block) * (base_color_dimension->y_num_block) / 8 + 1;//1021
    //bitstreamInitialize(&estimate_map, kEstimateMapInitialSize);

	estimate_map.data = aEstimate_map_data;//8160
    estimate_map.capacity = kEstimateMapInitialSize;
    estimate_map.length = 0;
    estimate_map.buffer = 0;
    estimate_map.buffer_size = 0;

    InputBitstream base_color_bitstream; //基本色数据码流
    const uint32_t kBaseColorBitstreamInitialSize =
            (base_color_dimension->x_num_block) * (base_color_dimension->y_num_block) * 3;//24480
    //bitstreamInitialize(&base_color_bitstream, kBaseColorBitstreamInitialSize);
	
	base_color_bitstream.data = aBase_color_bitstream_data;//24480
    base_color_bitstream.capacity = kBaseColorBitstreamInitialSize;
    base_color_bitstream.length = 0;
    base_color_bitstream.buffer = 0;
    base_color_bitstream.buffer_size = 0;

    for (y_index = 0; y_index < base_color_dimension->y_num_block; y_index++) {
        for (x_index = 0; x_index < base_color_dimension->x_num_block; x_index++) {
            int current_pos = y_index * base_color_dimension->x_num_block + x_index;      //鍍忕礌鎵�湪浣嶇疆
            int start_pos = max(y_index - 1, 0) * base_color_dimension->x_num_block + 0;  //鍍忕礌鎵�湪浣嶇疆涓婁竴琛岀殑绗竴涓儚绱�
            const struct BaseColor *base_color = base_color_map + current_pos;
            if (base_color->num_base_color != NON_TEXT_BASECOLOR_NUM_FLAG) {
                int result = checkBaseColorFitPrevious(base_color_map, start_pos, current_pos);
                if (result < 0) {
                    bitstreamPutBit(&estimate_map, 0);
                    codeBaseColor(base_color, num_base_color_in_mb, &base_color_bitstream);
                } else if (result >= 0) {
                    bitstreamPutBit(&estimate_map, 1);
                    bitstreamPutInt(&base_color_bitstream, ByteLength, (uint32_t) result);
                }
            } else {
                //非文字块，不写入码流
            }
        }
    }

    bitstreamFinalize(&estimate_map);
    bitstreamFinalize(&base_color_bitstream);

    //TEST code
    printf("is_basecolor_estimatable_map size: %d\n", estimate_map.length);
    printf("base_color_size: %d\n", base_color_bitstream.length);

    //预测索引图长度 3字节,8100字节
    bitstreamPutInt(bit_stream, ByteLength * 3, estimate_map.length);
    //基本色数据码流长度 3字节，24300字节
    bitstreamPutInt(bit_stream, ByteLength * 3, base_color_bitstream.length);

    //写入基本色预测索引图码流值至总码流//7M
    bitstreamPutCharArray(bit_stream, estimate_map.length, estimate_map.data);
    //写入基本色数据码流至总码流
    bitstreamPutCharArray(bit_stream, base_color_bitstream.length, base_color_bitstream.data);

    //释放内存
    //bitstreamDestroy(&estimate_map);
    //bitstreamDestroy(&base_color_bitstream);
}

static void codeBaseColor(const struct BaseColor *base_color, const int base_color_num,
                          InputBitstream *bit_stream)
{
	int i;
    //Base color: {Y1,Y2,..} 1 Byte/color
    for (i = 0; i < base_color_num; i++) {
        bitstreamPutInt(bit_stream, ByteLength, (uint32_t) base_color->component1[i]);
    }
    //Base color: {U1,U2,..} 1 Byte/color
    for (i = 0; i < base_color_num; i++) {
        bitstreamPutInt(bit_stream, ByteLength, (uint32_t) base_color->component2[i]);
    }
    //Base color: {V1,V2,..}  1 Byte/color
    for (i = 0; i < base_color_num; i++) {
        bitstreamPutInt(bit_stream, ByteLength, (uint32_t) base_color->component3[i]);
    }
}

static int checkBaseColorFitPrevious(const struct BaseColor *base_color_map,
                                     const int start_pos, const int current_pos)
{
	int index_to_check;
    const struct BaseColor *current_base_color = base_color_map + current_pos;
    for (index_to_check = start_pos; index_to_check < current_pos; index_to_check++) {
        const struct BaseColor *base_color_to_check = base_color_map + index_to_check;
        if (isBaseColorEqual(current_base_color, base_color_to_check)) {
            return index_to_check - start_pos;
        }
    }
    return -1;

}
