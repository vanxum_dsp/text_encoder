//
// Created by Derui on 2015/6/12.
//
#include "vgtp/codec/text_codec/encoder/TextEncoder.h"

#include "vgtp/codec/text_codec/TextCodecConfig.h"
#include "vgtp/codec/text_codec/encoder/Quantizer.h"
#include "vgtp/codec/text_codec/encoder/IndexEstimateCoder.h"
#include "vgtp/codec/text_codec/encoder/HierarchicalPatternCoder.h"
#include "vgtp/codec/text_codec/encoder/BitStreamGenerator.h"
#include "drive/mem_drive.h"
#include "var/var_text_encoder.h"
#include <time.h>

extern Pixel aQuantized_image[FRAME_WIDTH*FRAME_HEIGHT];
extern QuantizeIndex aQuantized_image_index[FRAME_WIDTH*FRAME_HEIGHT];
extern BaseColor aBase_color_map[8160];
extern EstimateSymbol aEstimate_symbols[FRAME_WIDTH*FRAME_HEIGHT];
/**
 * 分配内存：
 * bitstream的成员指针data
 *
 * 释放内存：
 * block_recognize_info指针
 */
int encodeText(const struct Block *tile, const struct QuantizeFactor *quantize_factor,
               const enum MBEncodeType *block_recognize_info,
               struct StreamStats *stream_stats, struct RawBitstream *bitstream, struct ResidualBlock *residual_block)
{

    /**
     * //TODO(derui) 在文字编码器开始执行前进行参数检查
     * 参数检查需求：
     * 分层模式编码：宏块数量需能被PIXEL_GROUP_COUNT整除。
     *             宏块大小需能被4整除
     */

    //获取宏块信息，为中间变量预先分配内存

    //计算宏块数量
	int i,j;
    const unsigned int tile_width = tile->width;
    const unsigned int tile_height = tile->height;

    struct BaseColorDimension base_color_dimension;
    base_color_dimension.x_num_block = (tile_width % BLOCK_SIZE == 0) ? tile_width / BLOCK_SIZE :
                                       tile_width / BLOCK_SIZE + 1;
    base_color_dimension.y_num_block = (tile_height % BLOCK_SIZE == 0) ? tile_height / BLOCK_SIZE :
                                       tile_height / BLOCK_SIZE + 1;

    const unsigned int num_pixels = tile_width * tile_height;
    const unsigned int num_blocks = base_color_dimension.x_num_block * base_color_dimension.y_num_block;

    //分配中间变量内存
    //struct Pixel *quantized_image = (struct Pixel *) Drv_calloc(num_pixels, sizeof(struct Pixel));
    //struct QuantizeIndex *quantized_image_index = (struct QuantizeIndex *) Drv_malloc(num_pixels * sizeof(struct QuantizeIndex));
	struct Pixel *quantized_image = aQuantized_image;//量化索引图
	struct QuantizeIndex *quantized_image_index = aQuantized_image_index;
    //将序列索引图初始化为一个不可用的值，如果该像素对应的块不是文字块，该不可用的值将保留
    for (j = 0; j < num_pixels; ++j) {
        quantized_image_index[j].component1 = NOT_TEXT_FILL_VALUE;
        quantized_image_index[j].component2 = NOT_TEXT_FILL_VALUE;
        quantized_image_index[j].component3 = NOT_TEXT_FILL_VALUE;
        //TODO(derui) 以下对quantized_image的赋值供测试使用
        quantized_image[j].component1 = -255;
        quantized_image[j].component2 = -255;
        quantized_image[j].component3 = -255;
    }

    //分配全图像所有宏块基本色信息的内存
    //struct BaseColor *base_color_map = (struct BaseColor *) Drv_calloc(num_blocks, sizeof(struct BaseColor));
	struct BaseColor *base_color_map = aBase_color_map;//aBase_color_map宏块基本色，4个基本色，每个基本色三个字节
    for (i = 0; i < num_blocks; i++) {
        base_color_map[i].num_base_color = BASE_COLOR_NUMBER;
        //base_color_map[i].component1 = (int *) Drv_calloc(BASE_COLOR_NUMBER, sizeof(int));
        //base_color_map[i].component2 = (int *) Drv_calloc(BASE_COLOR_NUMBER, sizeof(int));
        //base_color_map[i].component3 = (int *) Drv_calloc(BASE_COLOR_NUMBER, sizeof(int));
		base_color_map[i].component1 = 0;
        base_color_map[i].component2 = 0;
        base_color_map[i].component3 = 0;
    }

    unsigned int num_text_mb;
    //量化图像，获取量化后图像信息，量化索引图，基本色信息
    quantizeImage(tile, quantize_factor, &base_color_dimension, block_recognize_info, quantized_image,
                  quantized_image_index, base_color_map, residual_block, &num_text_mb);

    //对量化索引图进行2D预测生成预测索引图
    //enum EstimateSymbol *estimate_symbols = (enum EstimateSymbol *) Drv_malloc(num_pixels * sizeof(enum EstimateSymbol));
	enum EstimateSymbol *estimate_symbols = aEstimate_symbols;
    estimateQuantizedImageIndex(tile_width, tile_height, quantized_image_index, estimate_symbols);

    //Hierarchical Pattern encode
    int *pixel_group_level_coding_result;//char
    int size_pixel_group_level_coding_result;
    codeHierarchy(estimate_symbols, tile_width, tile_height,
                  &pixel_group_level_coding_result, &size_pixel_group_level_coding_result);

    //Encoder validity test
#if TEXT_ENCODER_TEST_ENABLED == 1
    textEncoderTest(tile, base_color_map, &base_color_dimension, quantized_image, quantized_image_index,
                    estimate_symbols, pixel_group_level_coding_result, size_pixel_group_level_coding_result,
                    residual_block);
#endif
    //Generate bit stream
    generateStream(num_pixels, num_text_mb, base_color_map, &base_color_dimension, quantized_image, estimate_symbols,
                   quantized_image_index, pixel_group_level_coding_result, size_pixel_group_level_coding_result,
                   bitstream, stream_stats);

    //释放内存
    Drv_free(quantized_image);
    Drv_free(quantized_image_index);
    for (i = 0; i < num_blocks; i++) {
        Drv_free(base_color_map[i].component1);
        Drv_free(base_color_map[i].component2);
        Drv_free(base_color_map[i].component3);
    }
    Drv_free(base_color_map);
    Drv_free(estimate_symbols);

    //释放输入参数block_recognize_info的内存
    return TEXT_ENCODE_SUCCESS;
}


static void textEncoderTest(const struct Block *tile, struct BaseColor *base_color_map,
                            const struct BaseColorDimension *base_color_dimension, const struct Pixel *quantized_image,
                            const struct QuantizeIndex *quantized_image_index,
                            const enum EstimateSymbol *estimate_symbols, const int *pixel_group_level_coding_result,
                            const int size_pixel_group_level_coding_result,
                            const struct ResidualBlock *residual_block) {
    const int width = tile->width;
    const int height = tile->height;
    const int tile_stride = tile->stride;
    const int size = width * height;

    const int start_column = width < 100 ? 0 : 160;
    const int end_column = 120 > width ? width : 165;
    const int start_row = height < 100 ? 0 : 115;
    const int end_row = 120 > height ? height : 120;
    int r,c;
    int i,j;
    FILE *log_file;
    const char *file_name = "D:\\vdin\\log_file";
    log_file = fopen(file_name, "a+");
    if (log_file == NULL) {
        fprintf(stderr, "Failed to open log input file.\n");
        return;
    }

    printf("Text codec debug is enabled. See log file at %s \n", file_name);

    time_t t;
    time(&t);
    fprintf(log_file, "============================\n");
    fprintf(log_file, "Current time: %s\n", ctime(&t));

    fprintf(log_file, "Picture size: %d(width)*%d(height)\n", width, height);

    fprintf(log_file, "Test area:\n");
    fprintf(log_file, "Column %d->%d, Row %d->%d\n", start_column, end_column,
           start_row, end_row);

    //Print picture YUV
    fprintf(log_file, "======Picture YUV=======\n");
    for (r = start_row; r < end_row; r++) {
        for (c = start_column; c < end_column; c++) {
            fprintf(log_file, "(%d,%d,%d)", tile->data_component1[r * tile_stride + c],
                    tile->data_component2[r * tile_stride + c],
                    tile->data_component3[r * tile_stride + c]);
        }
        fprintf(log_file, "\n");
    }

    //Print base color:
    fprintf(log_file, "======Picture base color======\n");
    const int num_block = base_color_dimension->x_num_block * base_color_dimension->y_num_block;
    for (i = 0; i < num_block; i++) {
        const int num_base_color = base_color_map[i].num_base_color;
        for (j = 0; j < num_base_color; j++) {
            fprintf(log_file, "Base color %d:(%d,%d,%d)\n", j, base_color_map[i].component1[j],
                    base_color_map[i].component2[j],
                    base_color_map[i].component3[j]);
        }
        fprintf(log_file, "\n");

    }

    fprintf(log_file, "======Quantized image======\n");
    for (r = start_row; r < end_row; r++) {
        for (c = start_column; c < end_column; c++) {
            fprintf(log_file, "(%d,%d,%d)", quantized_image[r * width + c].component1,
                    quantized_image[r * width + c].component2,
                    quantized_image[r * width + c].component3);
        }
        fprintf(log_file, "\n");
    }

    fprintf(log_file, "======Quantized image index======\n");
    for (r = start_row; r < end_row; r++) {
        for (c = start_column; c < end_column; c++) {
            fprintf(log_file, "(%d,%d,%d)", quantized_image_index[r * width + c].component1,
                    quantized_image_index[r * width + c].component2,
                    quantized_image_index[r * width + c].component3);
        }
        fprintf(log_file, "\n");
    }

    const int escape_count = BASE_COLOR_NUMBER;
    int y_escape_count = 0;
    int u_escape_count = 0;
    int v_escape_count = 0;
    for (i = 0; i < size; i++) {
        if (quantized_image_index[i].component1 == escape_count) {
            u_escape_count++;
        }
        if (quantized_image_index[i].component2 == escape_count) {
            y_escape_count++;
        }
        if (quantized_image_index[i].component3 == escape_count) {
            v_escape_count++;
        }
    }
    fprintf(log_file, "Quantized image index size:%d\n", size);

    fprintf(log_file, "y escape count:%d\n", y_escape_count);
    fprintf(log_file, "u escape count:%d\n", y_escape_count);
    fprintf(log_file, "v escape count:%d\n", y_escape_count);

    fprintf(log_file, "======Estimated image index======\n");
    for (r = start_row; r < end_row; r++) {
        for (c = start_column; c < end_column; c++) {
            fprintf(log_file, "%d ", estimate_symbols[r * width + c]);
        }
        fprintf(log_file, "\n");
    }

    fprintf(log_file, "======Pixel group level coding======\n");
    for (i = 0; i < size_pixel_group_level_coding_result; ++i) {
        fprintf(log_file, "%d ", pixel_group_level_coding_result[i]);
    }
    fprintf(log_file, "\n");

    fprintf(log_file, "======Residual block=======\n");
    unsigned int residual_block_stride = residual_block->stride;
    for (r = start_row; r < end_row; r++) {
        for (c = start_column; c < end_column; c++) {
            fprintf(log_file, "(%d,%d,%d)", residual_block->data_component1[r * residual_block_stride + c],
                    residual_block->data_component2[r * residual_block_stride + c],
                    residual_block->data_component3[r * residual_block_stride + c]);
        }
        fprintf(log_file, "\n");
    }
}
