/**
 * Copyright 2015 Xi'an Vanxum Electronics Technology Co.,Ltd. All Rights Reserved.
 *
 * Created by Derui on 2015/9/17.
 *
 * 定义码流统计数据
 */

#include "vgtp/codec/text_codec/encoder/StreamStats.h"

void addStats(struct StreamStats *total_stats, const struct StreamStats *appending_stats) {
    total_stats->size_base_color += appending_stats->size_base_color;
    total_stats->size_escape_color += appending_stats->size_escape_color;
    total_stats->size_o_symbol_code += appending_stats->size_o_symbol_code;
    total_stats->size_pixel_group_huffcode += appending_stats->size_pixel_group_huffcode;
}

int getTotalSize(const struct StreamStats *stats) {
    return (stats->size_base_color + stats->size_pixel_group_huffcode
            + stats->size_o_symbol_code + stats->size_escape_color) / 8;
}
