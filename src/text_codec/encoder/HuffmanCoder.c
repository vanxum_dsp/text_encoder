//
// Created by Derui on 2015/6/23.
//


#include "vgtp/codec/text_codec/encoder/HuffmanCoder.h"
#include "core/util/priority_queue.h"
#include "drive/mem_drive.h"

static struct INode *buildTree(const int *frequencies, const unsigned int frequency_count)
{
    /**
     * node1的frequency小于node2时, compareNodeFrequency返回正值，即node1有更高的优先级，会被优先取出
     */
	int i;
    PQueue *frequency_priority_queue = pqueue_new(compareNodeFrequency, frequency_count);

    //将所有频率(霍夫曼树的叶子节点)加入到有序队列中
    for (i = 0; i < frequency_count; ++i) {
        if (frequencies[i] != 0) {
            struct INode *leaf_node = createLeafNode(frequencies[i], i);
//            printf("enqueue leaf: %d\n", leaf_node->frequency);
            pqueue_enqueue(frequency_priority_queue, leaf_node);
        }
    }
    while (frequency_priority_queue->size > 1) {
//        printf("build trunk dequeue leaf\n");
        struct INode *child_right = pqueue_dequeue(frequency_priority_queue);

//        printf("build trunk dequeue leaf\n");
        struct INode *child_left = pqueue_dequeue(frequency_priority_queue);

        struct INode *parent = createTrunkNode(child_left, child_right);
//        printf("enqueue trunk: %d\n", parent->frequency);
        pqueue_enqueue(frequency_priority_queue, parent);
    }

//    printf("dequeue the last element\n");
    if (frequency_priority_queue->size == 1) {
    struct INode *root_node = pqueue_dequeue(frequency_priority_queue);
    pqueue_delete(frequency_priority_queue);
        return root_node;
    } else {
        return NULL;
    }
}

static struct INode *createLeafNode(const int frequency, const int value) {
    struct INode *new_node = (struct INode *) Drv_malloc(sizeof(struct INode));
    new_node->frequency = frequency;
    new_node->is_leaf = true;
    new_node->left = NULL;
    new_node->right = NULL;
    new_node->value = value;
    return new_node;
}

static struct INode *createTrunkNode(struct INode *left_node, struct INode *right_node) {
    struct INode *new_node = (struct INode *) Drv_malloc(sizeof(struct INode));
    new_node->is_leaf = false;
    new_node->left = left_node;
    new_node->right = right_node;
    new_node->value = -1;//useless
    new_node->frequency = left_node->frequency + right_node->frequency;
    return new_node;
}

static void generateCodes(const struct INode *node, const HuffCode *prefix, HuffCode *huff_code_dict) {
    if (node->is_leaf) {
        huff_code_dict[node->value] = cvector_copy(*prefix);
    } else {
        HuffCode left_prefix = cvector_copy(*prefix);
        int left_value = 0;
        cvector_pushback(left_prefix, &left_value);
        generateCodes(node->left, &left_prefix, huff_code_dict);

        HuffCode right_prefix = cvector_copy(*prefix);
        int right_value = 1;
        cvector_pushback(right_prefix, &right_value);
        generateCodes(node->right, &right_prefix, huff_code_dict);
    }
    cvector_destroy(*prefix);
}

struct INode *getHuffCodeMap(HuffCode *huff_code_dict, const int *frequencies, const unsigned int frequency_count) {
    struct INode *root = buildTree(frequencies, frequency_count);
    if (root) {
        cvector prefix = cvector_create(sizeof(unsigned int));
        generateCodes(root, &prefix, huff_code_dict);
    } else {
        printf("Nil root detected.\n");
    }
//    for (int i = 0; i < frequencyCounts; i++) {
//        printf("%d: %d\n", i, frequencies[i]);
//    }

//    for (HuffCodeMap::const_iterator it = huffCodeMap.begin(); it != huffCodeMap.end(); ++it) {
//        std::cout << it->first << " ";
//        std::copy(it->second.begin(), it->second.end(),
//                  std::ostream_iterator<bool>(std::cout));
//        std::cout << std::endl;
//    }
    return root;
}

void freeHuffmanTree(struct INode *root_node) {
    if (!root_node) {
        return;
    }

    if (root_node->is_leaf) {
        Drv_free(root_node);
    } else {
        freeHuffmanTree(root_node->left);
        freeHuffmanTree(root_node->right);
        Drv_free(root_node);
    }
}

/**
* 返回 0               - node1的frequency等于node2
*     positive value  - node1的frequency小于node2
*     negative value  - node1的frequency大于node2
*/
int compareNodeFrequency(const void *node1, const void *node2) {
    struct INode *inode1 = (struct INode *) node1;
    struct INode *inode2 = (struct INode *) node2;

    if (inode1->frequency == inode2->frequency) {
        return 0;
    } else if (inode1->frequency < inode2->frequency) {
        return 1;
    } else if (inode1->frequency > inode2->frequency) {
        return -1;
    }
    return 0;
}
