//
// Created by Derui on 2015/6/15.
//


#include "vgtp/codec/text_codec/encoder/Quantizer.h"
#include "drive/mem_drive.h"
#include <stdio.h>
#include <string.h>

void quantizeImage(const struct Block *tile, const struct QuantizeFactor *quantize_factor,
                   const struct BaseColorDimension *base_color_dimension, const enum MBEncodeType *block_recognize_info,
                   struct Pixel *quantized_image, struct QuantizeIndex *quantized_image_index,
                   struct BaseColor *base_color_map, struct ResidualBlock *residual_block, unsigned int *num_text_mb)
{
	unsigned int x_block_current_index,y_block_current_index;
    unsigned int tile_width = tile->width;
    unsigned int tile_height = tile->height;
    unsigned int stride = tile->stride;
    const int block_size = 16;  //TODO(derui) 使用预测模块数据

    const unsigned int x_num_block = base_color_dimension->x_num_block;
    const unsigned int y_num_block = base_color_dimension->y_num_block;

    //Divide picture by block and quantize
    unsigned int text_mb_count = 0;
    int block_index = 0;
    for (y_block_current_index = 0; y_block_current_index < y_num_block; y_block_current_index++) {
        for (x_block_current_index = 0; x_block_current_index < x_num_block; x_block_current_index++) {

            /**
             * 遍历所有图像宏块
             *
             * 1. 判断是否为文字块，如果不是不做操作
             * 2. 获取宏块首元素offset，长度宽度
             * 3. 使用offset数据，生成原始数据宏块指针，以及输出数据指针
             */
            struct MBInfo mb_info;
            bool success = getMBInfo(tile_width, tile_height, block_size, x_block_current_index, y_block_current_index,
                                     &mb_info);
            if (success == false) {
                fprintf(stderr, "Fatal: Failed to get mb info due to invalid parameters.\n");
                continue;
            }

            //原始图像宏块
            struct Block mb_block;
            mb_block.height = mb_info.height;
            mb_block.width = mb_info.width;
            mb_block.stride = stride;
            mb_block.color_space_format = tile->color_space_format;
            mb_block.data_component1 = tile->data_component1 + mb_info.first_element_offset;
            mb_block.data_component2 = tile->data_component2 + mb_info.first_element_offset;
            mb_block.data_component3 = tile->data_component3 + mb_info.first_element_offset;

            //残差图像宏块
            struct ResidualBlock mb_residual_block;
            mb_residual_block.height = mb_info.height;
            mb_residual_block.width = mb_info.width;
            mb_residual_block.stride = stride;
            mb_residual_block.color_space_format = residual_block->color_space_format;
            mb_residual_block.data_component1 = residual_block->data_component1 + mb_info.first_element_offset;
            mb_residual_block.data_component2 = residual_block->data_component2 + mb_info.first_element_offset;
            mb_residual_block.data_component3 = residual_block->data_component3 + mb_info.first_element_offset;

            //对宏块执行量化操作
            struct QuantizeIndex *const block_quantize_index_start =
                    quantized_image_index + mb_info.first_element_offset;
            struct Pixel *const block_quantized_image_start = quantized_image + mb_info.first_element_offset;
            struct BaseColor *const block_base_color = base_color_map + block_index;

            if (block_recognize_info[block_index] == kText) {
                text_mb_count++;
                quantizeBlock(&mb_block, block_base_color, quantize_factor->delta1, quantize_factor->delta2,
                              block_quantize_index_start, block_quantized_image_start, &mb_residual_block);
            } else {
                quantizeNonTextBlock(block_base_color, block_quantize_index_start, block_quantized_image_start,
                                     &mb_residual_block);
            }
            block_index++;
        }
    }
    *num_text_mb = text_mb_count;
}

static void quantizeBlock(const struct Block *macro_block, struct BaseColor *const base_color, const int delta1,
                          const int delta2, struct QuantizeIndex *const block_start_quantize_index,
                          struct Pixel *const block_start_quantized_pixel, struct ResidualBlock *mb_residual_block) {
    //获取宏块的基本色信息
	int height,width;
    calcYUVBaseColor(delta1, macro_block, base_color);


    //打印调试信息
//    printf("Get base color successfully.\n");
//    for (int i = 0; i < base_color->num_base_color; i++) {
//        printf("y%d:%d, u%d:%d, v%d:%d \n", i, base_color->component1[i], i, base_color->component2[i], i,
//               base_color->component3[i]);
//    }

    //遍历宏块所有像素, 更新量化索引图和量化图像信息
    int block_width = macro_block->width;
    int block_height = macro_block->height;
    unsigned int data_ptr_offset = 0;
    for (height = 0; height < block_height; height++) {
        for (width = 0; width < block_width; width++) {
            struct Pixel pixel;
            bool success = getPixelFromBlock(macro_block, width, height, &pixel);
            if (success == false) {
                exit(-1);
            }

            struct Pixel *quantized_pixel = block_start_quantized_pixel + data_ptr_offset;
            quantizePixel(&pixel, delta1, delta2, base_color, quantized_pixel,
                          block_start_quantize_index + data_ptr_offset);

            //获取并存取残差图像
            updateResidualPixelInBlock(&pixel, quantized_pixel, data_ptr_offset, mb_residual_block);
            data_ptr_offset++;
        }
        //注意量化索引和量化结果两个输出向量均使用与macro_block相同的stride
        data_ptr_offset = data_ptr_offset - block_width + macro_block->stride;
    }
}

static void quantizeNonTextBlock(struct BaseColor *const base_color,
                                 struct QuantizeIndex *const block_start_quantize_index,
                                 struct Pixel *const block_start_quantized_pixel,
                                 struct ResidualBlock *mb_residual_block) {
    //设置基本色信息, 基本色原有内存空间仍保留
	int height,width;
    base_color->num_base_color = NON_TEXT_BASECOLOR_NUM_FLAG;

    //遍历宏块所有像素, 更新量化索引图和量化图像信息
    const int block_width = mb_residual_block->width;
    const int block_height = mb_residual_block->height;
    const int stride = mb_residual_block->stride;

    unsigned int data_ptr_offset = 0;
    for (height = 0; height < block_height; height++) {
        for (width = 0; width < block_width; width++) {
            //设置量化结果信息
            struct Pixel *quantized_pixel = block_start_quantized_pixel + data_ptr_offset;
            quantized_pixel->component1 = 0;
            quantized_pixel->component2 = 0;
            quantized_pixel->component3 = 0;

            //设置量化索引信息
            struct QuantizeIndex *quantize_index = block_start_quantize_index + data_ptr_offset;
            quantize_index->component1 = NOT_TEXT_FILL_VALUE;
            quantize_index->component2 = NOT_TEXT_FILL_VALUE;
            quantize_index->component3 = NOT_TEXT_FILL_VALUE;

            //存取残差图像, 原始图像与量化图像均使用同一变量，使残差为1
            updateResidualPixelInBlock(quantized_pixel, quantized_pixel, data_ptr_offset, mb_residual_block);
            data_ptr_offset++;
        }
        //注意量化索引和量化结果两个输出向量均使用与macro_block相同的stride
        data_ptr_offset = data_ptr_offset - block_width + stride;
    }
}
#define COLOR_RANGE 256
int gaY_max[8];//32Byte
int gaU_max[8];
int gaV_max[8];
static void calcYUVBaseColor(const int delta1, const struct Block *macro_block,
                             struct BaseColor *base_color) {
    const int num_base_color = base_color->num_base_color;
    int height,width;
    int color,maxArrayIndex,u;
    //获取色彩分布直方图
    int comp1_count_array[COLOR_RANGE]; //Y
    int comp2_count_array[COLOR_RANGE]; //U
    int comp3_count_array[COLOR_RANGE]; //V

    memset(comp1_count_array, 0, sizeof(comp1_count_array));
    memset(comp2_count_array, 0, sizeof(comp2_count_array));
    memset(comp3_count_array, 0, sizeof(comp3_count_array));

    //遍历宏块所有像素,生成直方图
    int block_width = macro_block->width;
    int block_height = macro_block->height;
    int data_ptr_offset = 0;
    for (height = 0; height < block_height; height++) {
        for (width = 0; width < block_width; width++) {
            comp1_count_array[macro_block->data_component1[data_ptr_offset]]++;
            comp2_count_array[macro_block->data_component2[data_ptr_offset]]++;
            comp3_count_array[macro_block->data_component3[data_ptr_offset]]++;
            data_ptr_offset++;
        }
        data_ptr_offset = data_ptr_offset - block_width + macro_block->stride;
    }

    /*
 * 在有损量化情况下(delta!=0), 临近的高几率颜色不会都成为基本色，而是会简并为一个基本色(高几率颜色-简并颜色<delta1)
 * 从0-255，每range个颜色合并为一组进行几率测算，以确定简并基本色 ---注意这种算法得到的不是最优结论  by DeruiDeng
 */

    const int range = delta1 * 2 + 1;

    /**
     * 中间变量，记录循环过程中当前出现几率最高的颜色
     */
    /*int *y_max = (int *)Drv_malloc(num_base_color*sizeof(int));
	int *u_max = (int *)Drv_malloc(num_base_color*sizeof(int));
	int *v_max = (int *)Drv_malloc(num_base_color*sizeof(int));
	*/
 	int *y_max = gaY_max;
	int *u_max = gaU_max;
	int *v_max = gaV_max;
    
    memset(y_max, 0, sizeof(y_max));
    memset(u_max, 0, sizeof(u_max));
    memset(v_max, 0, sizeof(v_max));

    for (color = 0; color < COLOR_RANGE; color += range) {
        int y_range_color_freq_sum = sumUpCountArray(comp1_count_array, color, range, COLOR_RANGE);
        for (maxArrayIndex = 0; maxArrayIndex < num_base_color; maxArrayIndex++) {
            if (y_range_color_freq_sum > y_max[maxArrayIndex]) {
                for (u = num_base_color - 1; u > maxArrayIndex; u--) {
                    base_color->component1[u] = base_color->component1[u - 1];
                    y_max[u] = y_max[u - 1];
                }
                base_color->component1[maxArrayIndex] = color + delta1;
                y_max[maxArrayIndex] = y_range_color_freq_sum;
                break;
            }
        }

        int u_range_color_freq_sum = sumUpCountArray(comp2_count_array, color, range, COLOR_RANGE);
        for (maxArrayIndex = 0; maxArrayIndex < num_base_color; maxArrayIndex++) {
            if (u_range_color_freq_sum > u_max[maxArrayIndex]) {
                for (u = num_base_color - 1; u > maxArrayIndex; u--) {
                    base_color->component2[u] = base_color->component2[u - 1];
                    u_max[u] = u_max[u - 1];
                }
                base_color->component2[maxArrayIndex] = color + delta1;
                u_max[maxArrayIndex] = u_range_color_freq_sum;
                break;
            }
        }

        int v_range_color_freq_sum = sumUpCountArray(comp3_count_array, color, range, COLOR_RANGE);
        for (maxArrayIndex = 0; maxArrayIndex < num_base_color; maxArrayIndex++) {
            if (v_range_color_freq_sum > v_max[maxArrayIndex]) {
                for (u = num_base_color - 1; u > maxArrayIndex; u--) {
                    base_color->component3[u] = base_color->component3[u - 1];
                    v_max[u] = v_max[u - 1];
                }
                base_color->component3[maxArrayIndex] = color + delta1;
                v_max[maxArrayIndex] = v_range_color_freq_sum;
                break;
            }
        }
    }
	Drv_free(y_max);
	Drv_free(u_max);
	Drv_free(v_max);
}

static void quantizePixel(const struct Pixel *pixel, const int delta1, const int delta2,
                          const struct BaseColor *base_color, struct Pixel *quantized_pixel,
                          struct QuantizeIndex *quantize_index) {
    //Y
    quantizeValue(pixel->component1, delta1, delta2, base_color->num_base_color, base_color->component1,
                  &quantized_pixel->component1,
                  &quantize_index->component1);
    //U
    quantizeValue(pixel->component2, delta1, delta2, base_color->num_base_color, base_color->component2,
                  &quantized_pixel->component2,
                  &quantize_index->component2);
    //V
    quantizeValue(pixel->component3, delta1, delta2, base_color->num_base_color, base_color->component3,
                  &quantized_pixel->component3,
                  &quantize_index->component3);
}

static void updateResidualPixelInBlock(const struct Pixel *origin_pixel, const struct Pixel *quantized_pixel,
                                       const unsigned int offset, struct ResidualBlock *residual_block) {
    int *component1 = residual_block->data_component1 + offset;
    int *component2 = residual_block->data_component2 + offset;
    int *component3 = residual_block->data_component3 + offset;
    *component1 = origin_pixel->component1 - quantized_pixel->component1;
    *component2 = origin_pixel->component2 - quantized_pixel->component2;
    *component3 = origin_pixel->component3 - quantized_pixel->component3;
}

static void quantizeValue(const int input_value, const int delta1, const int delta2, const int baseSize,
                          const int *const baseValue, int *quantize_result, int *index_result)
{
	int i;
    bool yFoundBaseColor = false;
    for (i = 0; i < baseSize; i++) {
        if (abs(input_value - baseValue[i]) <= delta1) {
            //Pixel fits the base color.
            *quantize_result = baseValue[i];
            *index_result = i;
            yFoundBaseColor = true;
            break;
        }
    }
    if (!yFoundBaseColor) {
        *quantize_result = input_value / delta2 * delta2;
        *index_result = baseSize;
    }
//    std::cout << "Input value:" << inputValue << ",";
//    std::cout << "Base value:";
//    for (int i = 0; i < baseSize; i++) {
//        std::cout << baseValue[i] << ",";
//    }
//    std::cout << std::endl;
//    std::cout << "index:" << indexResult << ",";
//    std::cout << "Quantized value:" << quantizeResult << std::endl;
}


static int sumUpCountArray(const int *color_count_array, const int color, const int group_size,
                           const int count_array_size) {
	int i;
    int sum = 0;
    for (i = 0; i < group_size; i++) {
        if (color + i < count_array_size) {
            sum += (color_count_array[color + i]);
        } else {
            //End of array
            break;
        }
    }
    return sum;
}
