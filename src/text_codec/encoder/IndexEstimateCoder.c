//
// Created by Derui on 2015/6/19.
//


#include "vgtp/codec/text_codec/encoder/IndexEstimateCoder.h"

void estimateQuantizedImageIndex(const int image_width, const int image_height,
                                 const struct QuantizeIndex *const quantized_image_index_map,
                                 enum EstimateSymbol *estimated_symbols)
{
	int coord_x;
	int coord_y;

    //Loop through the image and generate the estimate index map
    for (coord_y = 0; coord_y < image_height; coord_y++) {
        for (coord_x = 0; coord_x < image_width; coord_x++) {
            const struct QuantizeIndex *quantize_index =
                    quantized_image_index_map + (coord_x + image_width * coord_y);
            if (!isIndexInTextBlock(quantize_index)) {
                *estimated_symbols = NonText;
            } else if (isIndexEqualToLeft(coord_x, quantize_index)) {
                *estimated_symbols = Left;
            } else if (isIndexEqualToUp(coord_y, image_width, quantize_index)) {
                *estimated_symbols = Up;
            } else {
                *estimated_symbols = Other;
            }
            estimated_symbols++;
        }
    }
}


static bool isIndexEqualToLeft(const int current_coord_x, const struct QuantizeIndex *const quantized_image_index) {
    if (current_coord_x == 0) {
        return false;
    } else {
        return isIndexEqual(quantized_image_index, quantized_image_index - 1);
    }
}

static bool isIndexEqualToUp(const int current_coord_y, const int image_width,
                             const struct QuantizeIndex *const quantized_image_index) {
    if (current_coord_y == 0) {
        return false;
    } else {
        return isIndexEqual(quantized_image_index, quantized_image_index - image_width);
    }
}


static bool isIndexEqual(const struct QuantizeIndex *index1, const struct QuantizeIndex *index2) {
    bool result = (index1->component1 == index2->component1) && (index1->component2 == index2->component2) &&
                  (index1->component3 == index2->component3);
    return result;
}

static bool isIndexInTextBlock(const struct QuantizeIndex *const quantized_image_index) {
    return (quantized_image_index->component1) !=
           NOT_TEXT_FILL_VALUE;
}
