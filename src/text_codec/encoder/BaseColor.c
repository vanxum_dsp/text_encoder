/**
 * Copyright 2015 Xi'an Vanxum Electronics Technology Co.,Ltd. All Rights Reserved.
 *
 * Created by Derui on 2015/9/17.
 *
 * 定义BaseColor相关函数
 */

#include "vgtp/codec/text_codec/encoder/BaseColor.h"

bool isBaseColorEqual(const struct BaseColor *base_color1, const struct BaseColor *base_color2) {
	int i;
    if (base_color1->num_base_color != base_color2->num_base_color) {
        return false;
    }
    for (i = 0; i < base_color1->num_base_color; i++) {
        if (base_color1->component1[i] != base_color2->component1[i]
            || base_color1->component2[i] != base_color2->component2[i]
            || base_color1->component3[i] != base_color2->component3[i]) {
            return false;
        }
    }
    return true;
}

