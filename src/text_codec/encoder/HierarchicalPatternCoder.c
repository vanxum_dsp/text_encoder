//
// Created by Derui on 2015/6/24.
//
#include "vgtp/codec/text_codec/encoder/HierarchicalPatternCoder.h"

#include <math.h>
#include <stdlib.h>
#include "vgtp/codec/text_codec/TextCodecConfig.h"
#include "drive/mem_drive.h"
#include "var/var_text_encoder.h"

extern int aCoding_result[FRAME_WIDTH/PIXEL_GROUP_COUNT*FRAME_HEIGHT];
extern int aPixel_group_level_coding_result[FRAME_WIDTH*FRAME_HEIGHT];

bool codeHierarchy(const enum EstimateSymbol *const estimate_image_symbol_map, const int image_width,
                   const int image_height,
                   int **pixel_group_level_coding_result_ptr, int *result_size)
{
	int i;
	int coord_y;
    const int kNumEstimateSymbolType = 3;  //estimate symbol有L，U和O三种取值
    const int kLinePatternCodingOffset = (int) pow(kNumEstimateSymbolType, PIXEL_GROUP_COUNT);

    //TODO(derui) 拖移至统一判断函数
    if (image_width % PIXEL_GROUP_COUNT != 0) {
        return false;
    }
    //为输出分配内存空间。分配内存时假定全部为分组匹配（没有发生行匹配），因此分配的内存肯定是够用的
    const int estimated_result_size = image_width / PIXEL_GROUP_COUNT * image_height;

    //int *coding_result = (int *) Drv_malloc(estimated_result_size * sizeof(int));
	int *coding_result = aCoding_result;
    unsigned int output_size = 0;
    int *output_ptr = coding_result;
    for (coord_y = 0; coord_y < image_height; coord_y++) {
        const enum EstimateSymbol *const line_start_ptr = estimate_image_symbol_map + coord_y * image_width;
        const enum EstimateSymbol *ref_ptr = line_start_ptr;

        //Line pattern fit
        int result = fitLinePattern(ref_ptr, image_width);
        if (result >= 0) {
            *output_ptr = kLinePatternCodingOffset + result;
            output_size++;
            output_ptr++;
        } else {
            //Pixel group pattern fit
            while (ref_ptr + PIXEL_GROUP_COUNT - line_start_ptr <= image_width) {
                if (*ref_ptr == NonText) {
                    //Pixel Group位于非文字块，直接跳过。
                    ref_ptr += PIXEL_GROUP_COUNT;
                    continue;
                }
                int value = 0;
                for (i = 0; i < PIXEL_GROUP_COUNT; i++) {
                    value += (ref_ptr[i] * (int) (pow(kNumEstimateSymbolType, i)));
                }
                *output_ptr = value;
                output_size++;
                output_ptr++;
                ref_ptr += PIXEL_GROUP_COUNT;
            }
        }
    }

    *result_size = output_size;
    if (output_size > 0) {
        //*pixel_group_level_coding_result_ptr  = Drv_realloc(coding_result, output_size * sizeof(int));
		*pixel_group_level_coding_result_ptr  = coding_result;
        if (!*pixel_group_level_coding_result_ptr) {
            //内存分配不成功
            return false;
        }
    } else {
        //无输出数据
        return false;
    }
    return true;

}

static int fitLinePattern(const enum EstimateSymbol *const line_start, const int line_length)
{
	int i;
    enum EstimateSymbol reference = line_start[0];
    for (i = 1; i < line_length; i++) {
        if (reference != line_start[i] || reference == NonText) {
            //Line pattern not fit
            return -1;
        }
    }
    return reference;
}
