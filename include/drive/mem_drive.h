/*
 * mem_drive.h
 *
 *  Created on: 2015-12-12
 *      Author: Qujia
 */

#ifndef MEM_DRIVE_H_
#define MEM_DRIVE_H_

void Drv_malloc_init(char *bottom, unsigned long size);
void Drv_malloc_dispose(void);
void *Drv_malloc (unsigned int size);
void *Drv_calloc (unsigned int n,unsigned int size);
void *Drv_realloc(void *ptr, unsigned int size);
void Drv_free (void *m);
void Drv_mark (void **ptr);
void Drv_release (void *ptr);
char *Drv_strdup(char const *str);

#endif /* MEM_DRIVE_H_ */
