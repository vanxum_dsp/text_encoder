/**
 * Copyright 2015 Xi'an Vanxum Electronics Technology Co.,Ltd. All Rights Reserved.
 *
 * Created by ChrisChung on 2015/11/2.
 *
 * 文件描述 
 */

#ifndef VPIXEL_PIXEL_H
#define VPIXEL_PIXEL_H

typedef struct YUVPixel {
    int Y;
    int U;
    int V;
} YUVPixel;
typedef struct RGBPixel {
    int R;
    int G;
    int B;
} RGBPixel;
typedef struct Pixel {
    int component1;
    int component2;
    int component3;
} Pixel;

#endif //VPIXEL_PIXEL_H
