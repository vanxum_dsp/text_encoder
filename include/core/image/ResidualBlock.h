/**
 * Copyright 2015 Xi'an Vanxum Electronics Technology Co.,Ltd. All Rights Reserved.
 *
 * Created by Derui on 2015/11/27.
 *
 * 声明残差图像数据结构
 */


#ifndef VPIXEL_RESIDUALBLOCK_H
#define VPIXEL_RESIDUALBLOCK_H

#include "Block.h"

struct ResidualBlock {
    enum ColorSpaceFormat color_space_format;
    unsigned int width;
    unsigned int height;
    int *data_component1;
    int *data_component2;
    int *data_component3;
    unsigned int stride;
};

/**
 * 为新建Block初始化内存空间,当前只支持YUV444格式
 */
void initializeResidualBlock(const enum ColorSpaceFormat format, const unsigned int width, const unsigned int height,
                             struct ResidualBlock *block);

bool getPixelFromResidualBlock(const struct ResidualBlock *block, const int coordinate_x, const int coordinate_y,
                               struct Pixel *output);

void freeResidualBlockMemory(struct ResidualBlock *block);

#endif //VPIXEL_RESIDUALBLOCK_H
