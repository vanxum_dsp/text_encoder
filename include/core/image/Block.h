/**
 * Copyright 2015 Xi'an Vanxum Electronics Technology Co.,Ltd. All Rights Reserved.
 *
 * Created by ChrisChung on 2015/11/2.
 *
 * 定义内存信息中包含的图像块信息。该结构体为编码模块的输入信息格式，图像块流在内存中按照YUV444格式
 */

#ifndef VPIXEL_BLOCK_H
#define VPIXEL_BLOCK_H



#include <core/util/color_space.h>
#include <core/image/Pixel.h>
#include <stdbool.h>

/**
 * 定义内存信息中包含的图像块信息。该结构体为编码模块的输入信息格式，图像块流在内存中按照YUV444格式
 *
 * color_space_format - 图像块格式
 * width - 图像块的宽度
 * height - 图像块的高度
 * data_component1 - 图像块首个元素的分量1内存指针
 * data_component2 - 图像块首个元素的分量2内存指针
 * data_component3 - 图像块首个元素的分量3内存指针
 * stride - 图像块相邻两行元素在内存数据中的跳跃值
 */
struct Block {
    enum ColorSpaceFormat color_space_format;
    unsigned int width;
    unsigned int height;
    unsigned char *data_component1;  //亮度分量1指针
    unsigned char *data_component2;  //色度分量2指针
    unsigned char *data_component3;  //色度分量3指针
    unsigned int stride;
};

/**
 * 宏块位置信息
 *
 * first_element_offset  宏块首元素相对于整幅图像首元素的位移量
 * width                 宏块宽度
 * height                宏块高度
 *
 */
struct MBInfo {
    unsigned int first_element_offset;
    unsigned int width;
    unsigned int height;
};

/**
 * 为新建Block初始化内存空间,当前只支持YUV444格式
 */
void initializeBlock(const enum ColorSpaceFormat format, const unsigned int width, const unsigned int height,
                     struct Block *block);

bool getPixelFromBlock(const struct Block *block, const int coordinate_x, const int coordinate_y,struct Pixel *output);

void freeBlockMemory(struct Block *block);

/**
 * 输入整幅图像的尺寸信息，分块大小，宏块横纵编号，获取宏块的具体尺寸和偏移量
 *
 * 输入：
 * total_width      图像数据宽度
 * total_height     图像数据高度
 * block_size       宏块大小
 * block_coord_x    宏块x坐标(同一行第几个宏块，从0开始)
 * block_coord_y    宏块y坐标(同一列第几个宏块，从0开始)
 *
 * 输出：
 * mb_info          宏块信息结构体
 *
 * 返回：
 * true             如果输出了有效的mb_info
 * false            无法输出有效的mb_info，该参数的值未被改变
 */

bool getMBInfo(const unsigned int total_width, const unsigned int total_height, const unsigned int block_size, const unsigned int block_coord_x,
               const unsigned int block_coord_y, struct MBInfo *mb_info);

#endif //VPIXEL_BLOCK_H
