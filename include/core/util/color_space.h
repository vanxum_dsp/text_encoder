/**
 * Copyright 2015 Xi'an Vanxum Electronics Technology Co.,Ltd. All Rights Reserved.
 *
 * Created by ChrisChung on 2015/11/2.
 *
 * 文件描述 
 */

#ifndef VPIXEL_COLOR_SPACE_H
#define VPIXEL_COLOR_SPACE_H

#include <core/image/Pixel.h>

#define MAX(a, b) (((a) > (b)) ? (a) : (b))
#define MIN(a, b) ( ((a) < (b)) ? (a) : (b) )

typedef  enum ColorSpaceFormat {
    kYUV420,
    kRGB,
    kYUV444,
    kYUV422
}ColorSpaceFormat;

void YUV444_TO_RGB(const  YUVPixel *yuv,RGBPixel *rgb);
int clip_sh_0_255(int in);

#endif //VPIXEL_COLOR_SPACE_H
