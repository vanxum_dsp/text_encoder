# ifndef __CVECTOR_H__
# define __CVECTOR_H__


# include <stdio.h>
# include <stdlib.h>
# include <string.h>

# define MIN_LEN 256
# define CVEFAILED  -1
# define CVESUCCESS  0
# define CVEPUSHBACK 1
# define CVEPOPBACK  2
# define CVEINSERT   3
# define CVERM       4
# define EXPANED_VAL 1
# define REDUSED_VAL 2

typedef void *citerator;
typedef struct _cvector *cvector;


cvector   cvector_create   (const size_t size                           );
void      cvector_destroy  (const cvector cv                            );
size_t    cvector_length   (const cvector cv                            );
int       cvector_pushback (const cvector cv, void *memb                );
int       cvector_popback  (const cvector cv, void *memb                );
size_t    cvector_iter_at  (const cvector cv, citerator iter            );
int       cvector_iter_val (const cvector cv, citerator iter, void *memb);
citerator cvector_begin    (const cvector cv                            );
citerator cvector_end      (const cvector cv                            );
citerator cvector_next     (const cvector cv, citerator iter            );
int       cvector_val_at   (const cvector cv, size_t index, void *memb  );
int       cvector_insert   (const cvector cv, citerator iter, void *memb);
int       cvector_insert_at(const cvector cv, size_t index, void *memb  );
int       cvector_rm       (const cvector cv, citerator iter            );
int       cvector_rm_at    (const cvector cv, size_t index              );

/**
 * 对cvector执行深拷贝并返回，由Derui添加本方法
 *
 * from 原cvector
 *
 * 返回：
 * cvector的深拷贝结果。
 *
 * 该函数会为返回值分配内存，需要执行cvector_destroy进行手动释放
 */
cvector cvector_copy(const cvector from);

/**
 * 将一个cvector的数据添加至另一个cvector尾部，由Derui添加本方法
 *
 * from - 需要添加的cvector
 * to   - 目标cvector，将from添加至to的尾部
 *
 * 返回：
 *      如果两个cvector的元素长度不同，或是参数不合法，返回失败
 *      否则返回成功
 */
int cvector_concat(const cvector to, const cvector from);

/* for test  */
void      cv_info          (const cvector cv                            );
void      cv_print         (const cvector cv                            );


#endif /* EOF file cvector.h */

