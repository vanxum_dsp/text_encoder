/**
 * Copyright 2015 Xi'an Vanxum Electronics Technology Co.,Ltd. All Rights Reserved.
 *
 * 作者：ssurui
 *
 * 定义预测模块结果数据结构
 */

#ifndef VPIXEL_RECOGNIZE_RESULT_H
#define VPIXEL_RECOGNIZE_RESULT_H
#include "drive/mem_drive.h"
#include <stdlib.h>

/**
 * RecognizeResult类定义了宏块预测结果
 */
enum PredictType {
    kIntra,
    kInter
};

enum MBEncodeType {
    kUnknown,
    kMvMatched,
    kUnchanged,
    kText,
    kPicture
};

struct MV {
    int x;
    int y;
};
struct RecognizeResult {


    enum PredictType tile_type;
    struct MV global_mv;
    int ref_frame_id;
    unsigned long int mb_in_row; //Tile中一行有多少宏块，TODO（ssurui）调试用
    unsigned long int mb_in_column; //Tile中一列有多少宏块，TODO（ssurui）调试用
    unsigned long int mb_count; //Tile中包含的宏块数量
    enum MBEncodeType *total_mb_type; //Tile中所有宏块的编码模式指针
};

static void ReleaseRecognizeResult(struct RecognizeResult *result) {
    //释放内存
    if (NULL != result->total_mb_type) {
        Drv_free(result->total_mb_type);
    }
}

#endif //SMAIN_RecognizeResult_H
