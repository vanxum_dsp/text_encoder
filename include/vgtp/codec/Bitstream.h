/**
 * Copyright 2015 Xi'an Vanxum Electronics Technology Co.,Ltd. All Rights Reserved.
 *
 * Created by 苏睿 on 15/9/14.
 *
 * 定义码流数据结构。PackageBitstream是打包模块反馈给SController的数据结构，EncoderRawBitstream
 *
 * 第一质量等级码流结构
 *
 * BitstreamMeta                码流元数据
 * BasicLevelBitstreamMeta      第一质量等级元数据
 * RecognizeBitstreamMeta       识别码流元数据
 * RecognizeBitstream           识别码流
 * TextBitstream                文字码流数据
 * JpegBitstream                JPEG码流数据
 *
 **/

#ifndef SMAIN_BITSTREAM_H
#define SMAIN_BITSTREAM_H

#include<stdint.h>

/**
 * 定义编码器的返回码流格式
 */
struct RawBitstream {
    uint32_t length;
    unsigned char *data;
};
#endif //SMAIN_BITSTREAM_H
