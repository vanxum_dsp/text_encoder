/**
 * Copyright 2015 Xi'an Vanxum Electronics Technology Co.,Ltd. All Rights Reserved.
 *
 * Created by Derui on 2015/9/17.
 *
 * 定义文字编码器的运行参数
 */



#ifndef SMAIN_TEXTCODECCONFIG_H
#define SMAIN_TEXTCODECCONFIG_H

//运行参数
#define BASE_COLOR_NUMBER 4
#define BLOCK_SIZE 16
#define PIXEL_GROUP_COUNT   4  //分层编码中作为一组的图像数目
#define QUANTIZE_BASE_COLOR_DELTA1 4 //基本色量化参数delta1
#define QUANTIZE_ESCAPE_COLOR_DELTA2 16 //逃逸色量化参数delta2

//调试参数
#define TEXT_ENCODER_TEST_ENABLED  0//运行文字编码器的测试函数 textEncoderTest()

#endif //SMAIN_TEXTCODECCONFIG_H
