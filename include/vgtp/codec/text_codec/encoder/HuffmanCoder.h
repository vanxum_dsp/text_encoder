//
// Created by Derui on 2015/6/23.
//

#ifndef TEXTENCODER_HUFFMANCODER_H
#define TEXTENCODER_HUFFMANCODER_H


#include <stdbool.h>
#include <stdint.h>
#include "core/util/cvector.h"
#include "vgtp/serializer/BitstreamGenerator.h"

typedef cvector HuffCode;

typedef struct {
    InputBitstream *input_bitstream;
    uint32_t bit_length;
} HuffCodeBit;

struct INode {
    int frequency;
    int value;
    bool is_leaf;
    struct INode *left;
    struct INode *right;
};

struct INode *getHuffCodeMap(HuffCode *huff_code_dict, const int *frequencies, const unsigned int frequency_count);

/*
 * 比较node的frequency
 *
 *     0               - node1的frequency等于node2
 *     positive value  - node1的frequency小于node2
 *     negative value  - node1的frequency大于node2
 */
int compareNodeFrequency(const void *node1, const void *node2);

/**
 * 释放huffman树所有节点的内存。
 *
 * root_node - Huffman根节点
 *
 * 注意：使用根节点以外的方式调用本方法会导致内存泄漏。
 **/
void freeHuffmanTree(struct INode *root_node);



/**
 * 创建一个叶子节点
 *
 * 返回创建的叶子节点，该指针为动态分配内存，需要手工释放。
 */
static struct INode *createLeafNode(const int frequency, const int value);

/**
 * 创建一个树干节点
 *
 * left_node   -  树干节点左儿子
 * right_node  -  树干节点右儿子
 *
 * 返回树干节点，该指针为动态分配内存，需要手工释放。
 */
static struct INode *createTrunkNode(struct INode *left_node, struct INode *right_node);

/**
 * 创建huffman树
 *
 * frequencies        - 不同取值的频率信息  frequencies[5]=1表明5的出现频率为1
 * frequencies_count  - 取值范围，例如，256代表取值范围是0-255
 */
static struct INode *buildTree(const int *frequencies, const unsigned int frequency_count);

static void generateCodes(const struct INode *node, const HuffCode *prefix, HuffCode *huff_code_dict);


#endif //TEXTENCODER_HUFFMANCODER_H
