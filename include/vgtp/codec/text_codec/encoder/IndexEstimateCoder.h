//
// Created by Derui on 2015/6/19.
//

#ifndef TEXTENCODER_INDEXCODER_H
#define TEXTENCODER_INDEXCODER_H


#include <stdbool.h>
#include "PixelGrid.h"

void estimateQuantizedImageIndex(const int image_width, const int image_height,
                                 const struct QuantizeIndex *const quantized_image_index_map,
                                 enum EstimateSymbol *estimated_symbols);

static bool isIndexEqualToLeft(const int current_coord_x, const struct QuantizeIndex *const quantized_image_index);

static bool isIndexEqualToUp(const int current_coord_y, const int image_width,
                             const struct QuantizeIndex *const quantized_image_index);

static bool isIndexEqual(const struct QuantizeIndex *index1, const struct QuantizeIndex *index2);

static bool isIndexInTextBlock(const struct QuantizeIndex *const quantized_image_index);

#endif //TEXTENCODER_INDEXCODER_H
