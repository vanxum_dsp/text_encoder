//
// Created by Derui on 2015/6/12.
//

#ifndef TEXTENCODER_TEXTBLOCKENCODER_H
#define TEXTENCODER_TEXTBLOCKENCODER_H


#include "BaseColor.h"
#include "StreamStats.h"
#include "../../Bitstream.h"
#include "../TextCodecConfig.h"
#include "vgtp/recognize/RecognizeResult.h"
#include "core/image/Block.h"
#include "core/image/ResidualBlock.h"
#include "PixelGrid.h"

#define TEXT_ENCODE_SUCCESS 0
#define TEXT_ENCODE_INVALID_PARAMETER 1

int encodeText(const struct Block *tile, const struct QuantizeFactor *quantize_factor,
                const enum MBEncodeType *block_recognize_info,
                struct StreamStats *stream_stats,
            struct RawBitstream *bitstream, struct ResidualBlock *residual_block);


#ifdef TEXT_ENCODER_TEST_ENABLED

static void textEncoderTest(const struct Block *tile, struct BaseColor *base_color_map,
                           const struct BaseColorDimension *base_color_dimension, const struct Pixel *quantized_image,
                           const struct QuantizeIndex *quantized_image_index,
                           const enum EstimateSymbol *estimate_symbols, const int *pixel_group_level_coding_result,
                           const int size_pixel_group_level_coding_result, const struct ResidualBlock *residual_block);

#endif

#endif //TEXTENCODER_TEXTBLOCKENCODER_H
