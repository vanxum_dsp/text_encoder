/**
 * Copyright 2015 Xi'an Vanxum Electronics Technology Co.,Ltd. All Rights Reserved.
 *
 * 作者：邓德瑞
 *
 * 声明基本色及基本方法
 */


#ifndef TEXTENCODER_QUANTIZEDFRAME_H
#define TEXTENCODER_QUANTIZEDFRAME_H

#include <stdbool.h>

#define NON_TEXT_BASECOLOR_NUM_FLAG -1

typedef struct BaseColor {
    int num_base_color;
    int *component1;
    int *component2;
    int *component3;
}BaseColor;

/**
 * 如果两个宏块的三个分量的基本色顺序和值完全相同，返回true
 */
bool isBaseColorEqual(const struct BaseColor *base_color1, const struct BaseColor *base_color2);

struct BaseColorDimension {
    unsigned int x_num_block;//120
    unsigned int y_num_block;//68
};

/**
 * 定义文字编码器量化参数
 *
 * delta1  基本色量化参数
 * delta2  逃逸色量化参数
 */
struct QuantizeFactor {
    int delta1;
    int delta2;
};

#endif //TEXTENCODER_QUANTIZEDFRAME_H
