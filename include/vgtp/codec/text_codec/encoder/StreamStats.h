/**
 * Copyright 2015 Xi'an Vanxum Electronics Technology Co.,Ltd. All Rights Reserved.
 *
 * Created by Derui on 2015/9/17.
 *
 * 声明码流统计数据
 */



#ifndef SMAIN_STREAMSTATS_H
#define SMAIN_STREAMSTATS_H
/**
 * 定义码流各组成部分的大小
 *
 * size_base_color 基本色信息占用码流
 * size_pixel_group_huffcode 对XYZ执行霍夫曼编码的占用码流
 * size_o_symbol_code 对2D预测信息中O的编码占用码流
 * size_escape_color  对逃逸色编码的占用码流
 */
struct StreamStats {
    int size_base_color;
    int size_pixel_group_huffcode;
    int size_o_symbol_code;
    int size_escape_color;
};

/**
 * 添加流统计数据
 */
void addStats(struct StreamStats *total_stats, const struct StreamStats *appending_stats);

/**
 * 获取当前流的大小(单位为字节)
 */
int getTotalSize(const struct StreamStats *stats);

#endif //SMAIN_STREAMSTATS_H
