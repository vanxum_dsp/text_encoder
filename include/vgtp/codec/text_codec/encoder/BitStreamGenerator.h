/**
 * Copyright 2015 Xi'an Vanxum Electronics Technology Co.,Ltd. All Rights Reserved.
 *
 * 作者：邓德瑞
 *
 * 声明码流生成相关方法
 */


#ifndef TEXTENCODER_BITSTREAMGENERATOR_H
#define TEXTENCODER_BITSTREAMGENERATOR_H

#include "BaseColor.h"
#include "HuffmanCoder.h"
#include "PixelGrid.h"
#include "StreamStats.h"
#include "vgtp/codec/Bitstream.h"
#include "core/util/cvector.h"
#include <core/image/Pixel.h>
#include <vgtp/serializer/BitstreamGenerator.h>

#define max(a, b) ( ((a) > (b)) ? (a) : (b) )
#define min(a, b) ( ((a) < (b)) ? (a) : (b) )

/**
 * 生成码流数据
 *
 * num_pixels            像素数目
 * base_color_map        宏块基本色指针（内存将被释放）
 * base_color_dimension  宏块每列每行个数
 * quantized_image       量化后图像（内存将被释放）
 * estimate_symbols      2D预测索引图（内存将被释放）
 * quantized_image_index 量化索引图（内存将被释放）
 * pixel_group_level..   分组模式数据（内存将被释放）
 * size_pixel_group_..   分组模式数据长度
 * bit_stream            输出码流信息指针（将分配动态内存）
 */
void generateStream(const int num_pixels, const unsigned int num_text_mb, const struct BaseColor *base_color_map,
                    const struct BaseColorDimension *base_color_dimension, const struct Pixel *quantized_image,
                    const enum EstimateSymbol *estimate_symbols, const struct QuantizeIndex *quantized_image_index,
                    const int *pixel_group_level_coding_result, const int size_pixel_group_level_coding_result,
                    struct RawBitstream *bitstream, struct StreamStats *stream_stats);

static void codeBaseColors(const struct BaseColor *base_color_map,
                           const struct BaseColorDimension *base_color_dimension, const int num_base_color_in_mb,
                           InputBitstream *bit_stream);

static void codeBaseColor(const struct BaseColor *base_color, const int base_color_num,
                          InputBitstream *bit_stream);

static int checkBaseColorFitPrevious(const struct BaseColor *base_color_map,
                                     const int start_pos, const int current_pos);

static void codeEscapePixel(const struct Pixel *quantized_image,
                            const struct QuantizeIndex *quantized_image_index, const int num_pixels,
                            const int escape_index, InputBitstream *bit_stream);

static void codeOtherInEstimateSymbols(const enum EstimateSymbol *estimate_symbols,
                                       const struct QuantizeIndex *quantized_image_index, const int num_pixels,
                                       const int escape_index, InputBitstream *bit_stream);

static void codePixelGroupLevelData(const int *pixel_group_level_coding_result,
                                    const int size_pixel_group_level_coding_result, InputBitstream *out_stream);

static void codeHuffDictionary(struct INode *huff_tree_root, InputBitstream *out_stream);

static void codeHuffBitStream(const int *pixel_group_level_coding_result,
                              const int size_pixel_group_level_coding_result, const HuffCode *huff_code_map,
                              InputBitstream *out_stream);

static void codeHuffNodeBinary(const struct INode *node, InputBitstream *bit_stream);

#endif //TEXTENCODER_BITSTREAMGENERATOR_H
