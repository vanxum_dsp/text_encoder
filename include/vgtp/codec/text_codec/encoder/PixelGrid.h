/**
 * Copyright 2015 Xi'an Vanxum Electronics Technology Co.,Ltd. All Rights Reserved.
 *
 * Created by Derui on 2015/9/18.
 *
 * 定义用于文字编码器的图像数据，索引图，预测索引图的网状结构
 */



#ifndef SMAIN_PIXEL_GRID_H
#define SMAIN_PIXEL_GRID_H

#include "stdbool.h"

//网状结构中不属于文字块的像素的默认值
#define NOT_TEXT_FILL_VALUE -1000

/**
 * Index of quantized image
 */
typedef struct QuantizeIndex {
    int component1;//char
    int component2;//char
    int component3;//char
}QuantizeIndex;

/**
 * 2D-estimate symbol  L/U/O, NonText如果是不属于文字块的无效值
 */
typedef enum EstimateSymbol {
    Left = 0, Up, Other, NonText//char
}EstimateSymbol;
#endif //SMAIN_PIXEL_GRID_H
