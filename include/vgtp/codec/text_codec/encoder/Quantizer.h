/**
 * Copyright 2015 Xi'an Vanxum Electronics Technology Co.,Ltd. All Rights Reserved.
 *
 * Created by Derui on 2015/9/17.
 *
 * 定义图像量化逻辑
 */


#ifndef TEXTENCODER_IMAGEQUANTIZER_H
#define TEXTENCODER_IMAGEQUANTIZER_H

#include "core/image/Block.h"
#include "PixelGrid.h"
#include "BaseColor.h"
#include "core/image/Pixel.h"
#include "core/image/ResidualBlock.h"
#include "vgtp/recognize/RecognizeResult.h"
/**
 * 对一幅图像进行量化，获取基本色信息，量化后的图像信息和量化索引图
 *
 * tile                    整幅图像的引用
 * quantize_factor         量化因子
 * base_color_dimension    宏块的数量
 * quantize_image          输出量化后图像信息引用，需预先分配等同于像素数量的内存
 * quantized_image_index   输出量化索引图信息，需预先分配等同于像素数量的内存
 * base_color_map          输出基本色信息，需预先分配等同于宏块数量的内存
 */
void quantizeImage(const struct Block *tile, const struct QuantizeFactor *quantize_factor,
                   const struct BaseColorDimension *base_color_dimension, const enum MBEncodeType *block_recognize_info,
                   struct Pixel *quantized_image, struct QuantizeIndex *quantized_image_index,
                   struct BaseColor *base_color_map, struct ResidualBlock *residual_block, unsigned int *num_text_mb);

/**
 * Get YUV base color
 */
static void calcYUVBaseColor(const int delta1, const struct Block *macro_block,
                             struct BaseColor *base_color);

static void quantizePixel(const struct Pixel *pixel, const int delta1, const int delta2,
                          const struct BaseColor *base_color, struct Pixel *quantized_pixel,
                          struct QuantizeIndex *quantize_index);

static void updateResidualPixelInBlock(const struct Pixel *origin_pixel, const struct Pixel *quantized_pixel,
                                       const unsigned int offset, struct ResidualBlock *residual_block);

static void quantizeValue(const int input_value, const int delta1, const int delta2, const int baseSize,
                          const int *const baseValue, int *quantize_result, int *index_result);

static void quantizeBlock(const struct Block *macro_block, struct BaseColor *base_color, const int delta1,
                          const int delta2, struct QuantizeIndex *const block_start_quantize_index,
                          struct Pixel *const block_start_quantized_pixel,
                          struct ResidualBlock *const mb_residual_block);

/**
 * 文字编码器对于非文字块的默认处理
 */
static void quantizeNonTextBlock(struct BaseColor *const base_color ,
                                 struct QuantizeIndex *const block_start_quantize_index ,
                                 struct Pixel *const block_start_quantized_pixel ,
                                 struct ResidualBlock *mb_residual_block);

static int sumUpCountArray(const int *color_count_array, const int color, const int group_size,
                           const int count_array_size);

#endif //TEXTENCODER_IMAGEQUANTIZER_H
