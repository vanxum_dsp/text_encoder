//
// Created by Derui on 2015/6/24.
//

#ifndef TEXTENCODER_HIERARCHICALPATTERNCODER_H
#define TEXTENCODER_HIERARCHICALPATTERNCODER_H


#include "PixelGrid.h"

/**
 * 分层编码，将2D预测图中每四个元素组合，生成一个供霍夫曼编码的数值
 *
 * estimate_image_symbol_map              输入 2D预测索引图指针
 * image_width                            输入 图像宽度, 图像宽度应能被一组元素数（默认为4）整除，否则可能发生内存越界
 * image_height                           输入 图像高度
 * pixel_group_level_coding_result_ptr    输出 供霍夫曼编码的数值指针的引用，注意本方法会为该指针分配内存，需要进行手工释放
 * result_size                            输出 供霍夫曼编码的数值指针分配的内存大小
 *
 * 返回
 *      true - 操作成功
 *      false - 操作失败，pixel_group_level_coding_result_ptr不会被分配内存，不需要手工释放
 */
bool codeHierarchy(const enum EstimateSymbol *estimate_image_symbol_map, const int image_width, const int image_height,
                   int **pixel_group_level_coding_result_ptr, int *result_size);

static int fitLinePattern(const enum EstimateSymbol *line_start, const int line_length);


#endif //TEXTENCODER_HIERARCHICALPATTERNCODER_H
