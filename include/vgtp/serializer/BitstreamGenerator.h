/**
 * Copyright 2015 Xi'an Vanxum Electronics Technology Co.,Ltd. All Rights Reserved.
 *
 * Created by Derui on 2015/10/28.
 *
 * 声明码流生成相关函数
 *
 * 使用方法:
 * InputBitstream input_bitstream;  //声明
 * bitstreamInitialize(&input_bitstream, 5);  //初始化内存空间
 * bitstreamPutBit(&input_bitstream, 1);      //存入一个bit
 * bitstreamPutInt(&input_bitstream,8,255);   //存入一个整数255，指定长度为8
 * bitstreamFinalize(&input_bitstream);       //完成码流写入，将缓冲区中未凑满一字节的数据写入至码流
 */


#ifndef VPIXEL_BITSTREAMGENERATOR_H
#define VPIXEL_BITSTREAMGENERATOR_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#define kCharLength  8

/**
 * data        实际码流指针
 * length      data中实际码流长度
 * capacity    为码流分配内存空间的大小
 * buffer      字符缓冲
 * buffer_size 缓冲比特数
 */
typedef struct {
    uint32_t capacity;
    unsigned char buffer;
    unsigned int buffer_size;
    uint32_t length;
    unsigned char *data;
} InputBitstream;

/**
 * 初始化bitstream
 *
 * bitstream         待初始化的码流对象
 * initial_capacity  码流对象初始分配空间大小(字节数)
 */
int bitstreamInitialize(InputBitstream *bitstream, const uint32_t initial_capacity);

/**
 * 删除bitstream中的动态分配数据
 */
int bitstreamDestroy(InputBitstream *bitstream);

/**
 * 删除bitstream中的动态分配数据
 */
int bitstreamDestroy(InputBitstream *bitstream);

/**
 * 结束bitstream写入，将缓冲区中未凑满一字节的数据写入至码流
 */
int bitstreamFinalize(InputBitstream *bitstream);

/**
 * 向码流写入一个bit
 *
 * bit_value    bit数值0 or 1
 */
int bitstreamPutBit(InputBitstream *bitstream, const int bit_value);

/**
 * 向码流写入指定bit长度的无符号int
 *
 * bitstream    目标码流对象
 * bit_length   写入码流的bit长度
 * val          待写入的int值
 */
int bitstreamPutInt(InputBitstream *bitstream, const unsigned int bit_length, const unsigned int val);

/**
 * 向码流写入指定bit长度的有符号int
 *
 * bitstream    目标码流对象
 * bit_length   写入码流的bit长度
 * val          待写入的int值
 */
int bitstreamPutSignedInt(InputBitstream *bitstream, const unsigned int bit_length, const int val);

int bitstreamPutCharArray(InputBitstream *bitstream, const unsigned int data_length_in_byte, const unsigned char *data);

static int flushBuffer(InputBitstream *bitstream);

static int extendBitstream(InputBitstream *bitstream);

#ifdef __cplusplus
}
#endif

#endif //VPIXEL_BITSTREAMGENERATOR_H
