/**
 * Copyright 2015 Xi'an Vanxum Electronics Technology Co.,Ltd. All Rights Reserved.
 *
 * Created by Derui on 2015/10/28.
 *
 * 声明码流读取相关函数
 */


#ifndef VPIXEL_BITSTREAMPARSER_H
#define VPIXEL_BITSTREAMPARSER_H

#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    unsigned char buffer;
    unsigned int buffer_size;
    uint32_t position;
    uint32_t length;
    unsigned char *data;
} OutputBitstream;

void outputBitstreamInitialize(unsigned char *data, uint32_t length, OutputBitstream *output_bitstream);

bool outputBitstreamGetIntUnsigned(OutputBitstream *bitstream, unsigned int bit_length, uint32_t *result);

bool outputBitstreamGetIntSigned(OutputBitstream *bitstream, unsigned int bit_length, int32_t *result);

bool outputBitstreamGetBit(OutputBitstream *bitstream, int *result);

static bool outputBitstreamLoadBuffer(OutputBitstream *bitstream);

#ifdef __cplusplus
}
#endif

#endif //VPIXEL_BITSTREAMPARSER_H
