/**
 * @file FramMeta.hpp
 * define functions of Frame Meta data
 */

#ifndef FRAME_META_HPP
#define FRAME_META_HPP

#include "stdio.h"
#include "stdlib.h"
#include <string.h>
#include "RecognizeCommon.h"


struct FrameMeta
{
	enum YuvFormat format;
	unsigned int frame_no;
	int frame_flag;  //if m_iIntraFrameFlag==1, this is a I frame
	unsigned int width;
	unsigned int height;

	int init_flag;
    unsigned char* data1;
    unsigned char* data2;
    unsigned char* data3;
};

int InitFrameMeta(struct FrameMeta *frame);
int ClearFrameMeta(struct FrameMeta *frame);
int AllocateFrameComponentData(struct FrameMeta *frame);

#endif   //FRAME_META_HPP
