/**
 * @file ReadEphemerisData.hpp
 * define functions of Read YUV Files
 */

#ifndef READ_YUV_DATA_HPP
#define READ_YUV_DATA_HPP

#include "RecognizeCommon.h"
#include "FrameMeta.h"

struct ReadYUVData
{
	enum YuvFormat format;
	unsigned int width;
	unsigned int height;

};

long CalculateYUVFileCounts(const char* input_yuv_filename, int width, int height, enum YuvFormat format);
int ReadOneFrameData(const char* input_yuv_filename, struct FrameMeta *frame, int frame_no, int width, int height, enum YuvFormat format);

#endif   //READ_YUV_DATA_HPP
