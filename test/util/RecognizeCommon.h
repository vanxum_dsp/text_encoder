/*
 * Copyright 2015 Xi'an Vanxum Electronics Technology Co.,Ltd. All Rights Reserved.
 *
 * 作者：ssurui
 *
 * 定义识别模块常用的常数和宏
 */
#ifndef RECOGNIZE_COMMON_HPP
#define RECOGNIZE_COMMON_HPP

#define MB_WIDTH                                                                                      16
#define MB_HEIGHT                                                                                     16
#define HASH_TABLE_SIZE                                                                            65536
#define FEATURE_LENGTH 													  	                          32
#define I_INTERVAL                                                                                    30
#define MIN_REORDER_MB_COUNT                                                                          25
#define MIN_WAVELET_MB_COUNT                                                                         256
//Y分量的比特数
#define Y_BITS                                                				                           8
//出现最高频次的mv占比超过该值，认为该mv是全局mv
#define GLOBAL_MV_RATIO_THRESHOLD                                                                  0.125
//如果Inter帧中kMvMatched、kUnchanged这两种模式所占的比例很低，
//就可以判定发生了场景切换(Scene cut)
#define SCENE_CUT_RATIO_THRESHOLD                                                                   0.15

//DEBUG MACROS
#define DEBUG_SHOW_MATLAB
#ifdef DEBUG_SHOW_MATLAB
#define RECORD_INFORMATION_FRAME_NUM                                                                   0

#define RECORD_ALL_FEATURES_IN_A_FRAME                                                                 1
#define RECORD_ALL_FEATURES_FILENAME                                                    "./Features.txt"

#define RECORD_HASH_TO_FEATURES_INDEX_IN_A_TILE                                                        1
#define RECORD_HASH_TO_FEATURES_INDEX_FILENAME                                     "./Hash2Features.txt"

#define RECORD_COLLISION_FEATURES_IN_A_FRAME                                                           1
#define RECORD_COLLISION_FEATURES_FILENAME                                     "./CollisionFeatures.txt"

#define RECORD_MATCHED_FEATURES_IN_A_FRAME                                                             1
#define RECORD_MATCHED_FEATURES_FILENAME                                         "./MatchedFeatures.txt"

#define RECORD_ALL_MB_TYPES_IN_A_TILE                                                                  1
#define RECORD_AlL_MB_TYPES_FILENAME                                                     "./MBTypes.txt"

#define RECORD_ALL_MB_TYPES_IN_A_TILE_IN_INTRA_MESSAGE                                                 1
#define RECORD_AlL_MB_TYPES_FILENAME_IN_INTRA_MESSAGE                        "./MBTypesIntraMessage.txt"
#define RECORD_AlL_MB_TYPES_FILENAME_IN_INTRA_MESSAGE_DSP_MODE        "./MBTypesIntraMessageDspMode.txt"

#define RECORD_ALL_MB_TYPES_IN_A_TILE_IN_INTER_MESSAGE_NO_SCENE_CUT                                    1
#define RECORD_AlL_MB_TYPES_FILENAME_IN_INTRA_MESSAGE_WITHOUT_MV    "./MBTypesInterMessageWithoutMV.txt"
#define RECORD_AlL_MB_TYPES_FILENAME_IN_INTRA_MESSAGE_WITH_MV          "./MBTypesInterMessageWithMV.txt"
#define RECORD_AlL_MB_TYPES_FILENAME_IN_INTRA_MESSAGE_WITHOUT_MV_DSP_MODE    "./MBTypesInterMessageWithoutMVDspMode.txt"
#define RECORD_AlL_MB_TYPES_FILENAME_IN_INTRA_MESSAGE_WITH_MV_DSP_MODE          "./MBTypesInterMessageWithMVDspMode.txt"

#define RECORD_ALL_MB_TYPES_IN_A_TILE_IN_INTER_MESSAGE_WITH_SCENE_CUT                                  1
#define RECORD_AlL_MB_TYPES_FILENAME_IN_INTRA_MESSAGE_WITH_SCENE_CUT "./MBTypesInterMessageSceneCut.txt"
#define RECORD_AlL_MB_TYPES_FILENAME_IN_INTRA_MESSAGE_WITH_SCENE_CUT_DSP_MODE "./MBTypesInterMessageSceneCutDspMode.txt"

#endif

enum YuvFormat
{
	undefinedFormat,			///< Unknown or undefined YUV file format
	YUV444, 			///< YUV4:4:4
	YUV422, 			///< YUV4:2:2
	YUV420				///< YUV4:2:0
};

struct FeatureMeta //定义特征数据结构
{
    int hash_value;  //特征的hash值
    int x;
    int y;
};

struct HashMeta //store the location(x,y) of the feature, the structure are for fast match features
{
    unsigned int feature_vector_index;  //该hash值对应的特征向量的索引
    int x;
    int y;
};

struct MatchedFeatureMeta //匹配特征的信息
{
    int x; //matched feature position X
    int y; //matched feature position Y
    int mv_x;  //对应的运动向量x分量
    int mv_y; //对应的运动向量y分量
    int hash_value; //对应的Hash值
};

struct MvMeta // 用于统计匹配特征对应的MV信息，以便找到最可能的全局运动向量
{
    int mv_x;
    int mv_y;
    int count; //该mv在匹配特征vector中出现的次数
};

struct GlobalMvMeta //求出的全局mv的信息
{
    int mv_x;
    int mv_y;
    int valid_flag; //如果是1, 该mv是有效地全局运动向量
};

enum MBPosition
{
	kUndefined,			///< Unknown or undefined MB position
	kUpLeft, 		    ///< the MB with location in the Up Left
	kUp, 				///< the MB with location in the Up
	kUpRight,			///< the MB with location in the Up Right
	kLeft          		///< the MB with location in the Left
};

#endif   //RECOGNIZE_COMMON_HPP
