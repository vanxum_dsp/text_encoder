#include <stdio.h>
#include "ReadYUVData.h"

long CalculateYUVFileCounts(const char* input_yuv_filename, int width, int height, enum YuvFormat format)
{
	FILE * pFile;
	long size = -1;
	
	pFile = fopen(input_yuv_filename , "rb");
	if (pFile == NULL)
	{
		printf("Error opening file %s", input_yuv_filename);
	}
	else
	{
		fseek(pFile , 0 , SEEK_END );
		size=ftell (pFile); 
	}
	fclose (pFile);

	long frame_count = -1;
	if(YUV444 == format)
	{
        frame_count = size/(width*height*3);
	}
	else
	{
		printf("Unsupported YUV format.\n");
	}
	
    printf("size of %s is %lu bytes, it contains %lu frames.\n", input_yuv_filename, size, frame_count);
    return frame_count;
}

int ReadOneFrameData(const char* input_yuv_filename, struct FrameMeta *frame, int frame_no, int width, int height, enum YuvFormat format)
{
	int success = -1;

	//set frameMeta member
	frame->format = format;
	frame->frame_no= frame_no;
	if(0 == frame_no%I_INTERVAL)
	{
		frame->frame_flag = 1; //This frame is a I frame
	}
	else
	{
        frame->frame_flag = -1;
	}
	frame->width = width;
	frame->height = height;

	//check whether frameMeta has been allocated memory
	if(1 != frame->init_flag)
	{
		success = AllocateFrameComponentData(frame);
	}
	if (-1 != success)
	{
		FILE * pFile;
		int skipSize = 0;
		int componet1ReadSize = 0;
		int componet2ReadSize = 0;
		int componet3ReadSize = 0;

		if(YUV444 == format)
		{
			skipSize = width*height*3*frame_no;
			componet1ReadSize = width*height;
			componet2ReadSize = width*height;
			componet3ReadSize = width*height;
		}
		else
		{
			printf("Unsupported YUV format, read frame data error. \n");
			return -1;
		}

		pFile = fopen(input_yuv_filename , "rb");
		if (pFile == NULL)
		{
			printf("Error opening file |%s|. \n", input_yuv_filename);
			success = -1;
		}
		else
		{
			fseek(pFile , skipSize , SEEK_SET);
			
			// copy the file into the buffer:
			size_t result1 = fread (frame->data1,1,componet1ReadSize,pFile);
			size_t result2 = fread (frame->data2,1,componet2ReadSize,pFile);
			size_t result3 = fread (frame->data3,1,componet3ReadSize,pFile);
			
			if ((result1 != componet1ReadSize)||(result2 != componet2ReadSize)||(result3 != componet3ReadSize))
			{
				printf("Reading error.\n");
				success = -1;
			}
			else
			{
				printf("Reading one frame data from YUV file success.\n");
				success = 0;
			}
		}
		fclose (pFile);
	}
	return success;
}

