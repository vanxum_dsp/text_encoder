set(source_files
        FrameMeta.c
        ReadYUVData.c
        )
add_library(codec_test_utils ${source_files})
include_directories(./)
target_link_libraries(codec_test_utils core)