#include "FrameMeta.h"
#include "drive/mem_drive.h"
#include "var/var_text_encoder.h"

extern unsigned char frame_data1[FRAME_WIDTH*FRAME_HEIGHT];
extern unsigned char frame_data2[FRAME_WIDTH*FRAME_HEIGHT];
extern unsigned char frame_data3[FRAME_WIDTH*FRAME_HEIGHT];

int InitFrameMeta(struct FrameMeta *frame)
{
	frame->format = YUV444;
    frame->frame_no = 0;
    frame->init_flag = -1;
    frame->width = FRAME_WIDTH;
    frame->height = FRAME_HEIGHT;

	//Y, U, V component data
    frame->init_flag = -1;
    frame->data1 = NULL;
    frame->data2 = NULL;
    frame->data3 = NULL;
    return 0;
}

int ClearFrameMeta(struct FrameMeta *frame)
{
	if(NULL != frame->data1)
	{
		Drv_free(frame->data1);
        frame->data1 = NULL;
	}

	if(NULL != frame->data2)
	{
		Drv_free(frame->data2);
        frame->data2 = NULL;
	}

	if(NULL != frame->data3)
	{
		Drv_free(frame->data3);
        frame->data3 = NULL;
	}
	return 0;
}

int AllocateFrameComponentData(struct FrameMeta *frame)
{
	int success = -1;
	int width = frame->width;
    int height = frame->height;
	if(YUV444 == frame->format)
	{
		//frame->data1 = (unsigned char*)Drv_malloc(width*height);
        //frame->data2 = (unsigned char*)Drv_malloc(width*height);
        //frame->data3 = (unsigned char*)Drv_malloc(width*height);

		frame->data1 = (unsigned char*)frame_data1;
        frame->data2 = (unsigned char*)frame_data2;
        frame->data3 = (unsigned char*)frame_data3;

		if ((NULL != frame->data1) && (NULL != frame->data2) && (frame->data3))
		{
			success = 0;
			memset(frame->data1, 0, width*height);
			memset(frame->data2, 0, width*height);
			memset(frame->data3, 0, width*height);
		}
	}
	else
	{
		printf("Unsupported YUV format, allocate memory fail.\n");
	}

	if(-1 == success)
	{
		printf("Allocate YUV component memory fail.\n");
	}
	else
	{
		frame->init_flag = 1;
		printf("Allocate YUV component memory success.\n");
	}

    return success;
}


