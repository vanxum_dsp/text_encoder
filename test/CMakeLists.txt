add_subdirectory(util)

set(source_files
  main.c
)
add_executable(text_encoder_test ${source_files})
target_link_libraries(text_encoder_test core textcodec codec_test_utils)