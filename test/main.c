/**
 * Copyright 2015 Xi'an Vanxum Electronics Technology Co.,Ltd. All Rights Reserved.
 *
 * Created by Derui on 2015/11/6.
 *
 * 对文字编码器进行测试
 */



#include <stdbool.h>

#include "util/ReadYUVData.h"
#include "vgtp/codec/text_codec/encoder/TextEncoder.h"
#include "drive/mem_drive.h"
#include "var/var_text_encoder.h"

extern enum MBEncodeType mb_recognize_info[8160];//宏块为16*16, 1080p图像共8160个宏块
extern char Mem_malloc[0x40000000];
static bool readYUVBlock(const char *yuv_filename, const unsigned int width, const unsigned int height,
                        struct Block *block) {
    enum YuvFormat format = YUV444;
    struct FrameMeta *frame;// = (struct FrameMeta *) Drv_malloc(sizeof(struct FrameMeta));
    InitFrameMeta(frame);

    int success = ReadOneFrameData(yuv_filename, frame, 0, width, height, format);

    if (success == -1) {
        return false;
    }

    block->data_component1 = frame->data1;
    block->data_component2 = frame->data2;
    block->data_component3 = frame->data3;
    block->stride = width;
    block->width = width;
    block->height = height;
    block->color_space_format = kYUV444;
    return true;
}

static void testEncodeTile(const char *file_name, unsigned int start_pos)
{
	int i;
    const unsigned int image_width = FRAME_WIDTH;
    const unsigned int image_height = FRAME_HEIGHT;
    struct Block block;
    if (!readYUVBlock(file_name, image_width, image_height, &block)) {
        printf("Failed read YUV block.\n");
        return;
    }

    struct StreamStats stats;
    struct RawBitstream raw_bitstream;
    struct ResidualBlock residual_block;
    initializeResidualBlock(kYUV444, image_width, image_height, &residual_block);

    struct QuantizeFactor quantize_factor;
    quantize_factor.delta1 = 4;
    quantize_factor.delta2 = 16;
	
    //enum MBEncodeType mb_recognize_info[8160]; //宏块为16*16, 1080p图像共8160个宏块
	for (i = 0; i < 8160; i++) {
        mb_recognize_info[i] = kText;
    }
    encodeText(&block, &quantize_factor, mb_recognize_info, &stats, &raw_bitstream, &residual_block);

    printf("Encode text finished.\n");
}

int main(int argc, char **argv) {
    char file_name[200];
    memset(file_name, 0, 200);
    strcpy(file_name, "../docs/pdf.yuv");
	char *pbottom = Mem_malloc;
	unsigned long length = 0x30000000;
	Drv_malloc_init(pbottom, length);
    unsigned int start_pos = 0;
    testEncodeTile(file_name, start_pos);
}


